package me.cybersensei.simplerpg.lib;

/**
 * BattleResult is the result of some action in a battle. It knows what action was taken, who originated an action, who
 * the target was if any, and if an action was successful.
 *
 * @author Christopher Pridgen
 */
public class BattleResult {
    // This is the action that was attempted
    private Action action;

    // This is the origin of the action
    private String origin;

    // This is the target of the action if there was a target
    private String target;

    // This is the message that is displayed
    private String message;

    // This indicates whether the action was successful or not
    private boolean success;

    @Override
    public String toString() {
        return this.message;
    }

    /**
     * This acts like a constructor for a BattleResult from an Action.ATTACK.
     *
     * @param origin  This is the attacker.
     * @param target  This is the defender.
     * @param success This indicates whether the attacker was successful or not.
     * @param message This is the message to be displayed to the player.
     */
    public void attack(String origin, String target, boolean success, String message) {
        this.action = Action.ATTACK;
        this.origin = origin;
        this.target = target;
        this.success = success;
        this.message = message;
    }

    /**
     * This acts like a constructor for a BattleResult from an Action.VICTORY.
     *
     * @param origin  This is the winner of the battle.
     * @param success This indicates whether the victory was successful or not.
     * @param message This is the message to display to the player.
     */
    public void defeat(String origin, boolean success, String message) {
        this.action = Action.VICTORY;
        this.origin = origin;
        this.target = null;
        this.success = success;
        this.message = message;
    }

    /**
     * This acts like a constructor for a BattleResult from an Action.FLEE.
     *
     * @param origin  This is the coward.
     * @param success This indicates whether the flee was successful or not.
     * @param message This is the message to display to the player.
     */
    public void flee(String origin, boolean success, String message) {
        this.action = Action.FLEE;
        this.origin = origin;
        this.target = null;
        this.success = success;
        this.message = message;
    }

    /**
     * Get the action.
     *
     * @return This returns the attempted Action.
     */
    public Action getAction() {
        return this.action;
    }

    /**
     * Get the origin.
     *
     * @return This is the origin. This should not be null for a valid BattleAction.
     */
    public String getOrigin() {
        return this.origin;
    }

    /**
     * Get the target.
     *
     * @return This is the target. Only Action.ATTACK currently has a target so this will return null for all other
     * actions.
     */
    public String getTarget() {
        return this.target;
    }

    /**
     * Indicates if this was a successful action or not.
     *
     * @return This returns true to indicate success and false to indicate failure.
     */
    public boolean isSuccess() {
        return this.success;
    }


    /**
     * Get the message.
     *
     * @return This returns the message.
     */
    public String getMessage() {
        return this.message;
    }

    /**
     * This resets the BattleResult to a "clean" state. Origin, target, action, and message are all set to null and
     * success is set to false.
     */
    public void reset() {
        this.origin = null;
        this.target = null;
        this.action = null;
        this.message = null;
        this.success = false;
    }

    /**
     * These are the valid actions.
     */
    public enum Action {
        ATTACK,
        FLEE,
        VICTORY,
    }
}
