package me.cybersensei.simplerpg;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;
import me.cybersensei.simplerpg.lib.Player;

import java.util.Locale;

/**
 * This activity is displayed when the user had won a battle. It shows the spoils (gold and experience) and also will
 * inform the player if they levelled up.
 *
 * @author Christopher Pridgen
 */
public class VictoryActivity extends Activity implements View.OnClickListener {
    // The tag for logging
    private static final String TAG = VictoryActivity.class.getSimpleName();

    /**
     * This is used to store and retrieve the gold value from the Intent that spawned this activity. This is required to
     * be present in the Intent.
     *
     * @see android.content.Intent
     */
    public static final String GOLD = "gold";

    /**
     * This is used to store and retrieve the exp value from the Intent that spawned this activity. This is required to
     * be present in the Intent.
     *
     * @see android.content.Intent
     */
    public static final String EXP = "exp";

    @Override
    protected void onCreate(Bundle bundle) {
        // Initialize the activity
        super.onCreate(bundle);
        setContentView(R.layout.activity_victory);

        // Get the gold and exp values from the intent
        int goldAddition = getIntent().getIntExtra(GOLD, 0);

        int expAddition = getIntent().getIntExtra(EXP, 0);

        if (BuildConfig.DEBUG) {
            Log.d(TAG, String.format(Locale.US, "Player earned %d gold and %d exp",
                    goldAddition, expAddition));
        }

        // Find children
        TextView gold = findViewById(R.id.tv_gold_earned_value);
        TextView exp = findViewById(R.id.tv_exp_earned_value);
        TextView leveledUp = findViewById(R.id.tv_leveled_up);

        // Update TextView values
        gold.setText(String.format(Locale.getDefault(),
                getResources().getString(R.string.gold_earned_format_string), goldAddition));
        exp.setText(String.format(Locale.getDefault(),
                getResources().getString(R.string.exp_earned_format_string), expAddition));

        // Get the player
        Player player = Static.game.getPlayer();

        // Add gold to the player
        player.addGold(goldAddition);

        // Add experience to the player
        if (player.addExp(expAddition) > 0) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Player levelled up");
            leveledUp.setVisibility(View.VISIBLE);
        }

        // Set listeners
        findViewById(R.id.b_done).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.victory, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_done:
                finish();
                break;
            default:
                break;
        }
    }

    /**
     * This is the preferred was to start a VictoryActivity. It creates the Intent required and fills it with the needed
     * values.
     *
     * @param context This is the parent context.
     * @param gold    This is the amount of gold earned.
     * @param exp     This is the amount of exp earned.
     */
    public static void startVictoryActivity(Context context, int gold, int exp) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Request start VictoryActivity to award " + gold + " gold and " + exp + " exp");
        }

        // Create an intent
        Intent intent = new Intent(context, VictoryActivity.class);

        // Put values into the intent
        intent.putExtra(GOLD, gold);
        intent.putExtra(EXP, exp);

        // Start the activity
        context.startActivity(intent);
    }
}
