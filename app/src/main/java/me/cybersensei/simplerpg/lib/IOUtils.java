package me.cybersensei.simplerpg.lib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * This class houses static methods that are useful for IO purposes.
 *
 * @author Christopher Pridgen
 */
public class IOUtils {
    /**
     * This will read the entirety of an input stream and return the contents as a string.
     *
     * @param inputStream This is the input stream to read until completion. For best performance consider passing an
     *                    InputStream that has been wrapped in a BufferedInputStream.
     * @return This is the contents of the input stream as a string.
     * @throws IOException This is thrown if there are any problems reading from the stream.
     */
    public static String readAllString(InputStream inputStream) throws IOException {
        // Create a byte array output stream to collect the output
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        // Copy from the input stream to the output stream
        copy(inputStream, outputStream);

        // Return the contents of the output stream
        return outputStream.toString();
    }

    /**
     * This reads an entire input stream a parses a JSON with the contents.
     *
     * @param inputStream This is the input stream to read until completion. For best performance consider passing an InputStream that has been wrapped in a BufferedInputStream.
     * @return This is the JSON data after parsing.
     * @throws IOException   This is thrown if there are any problems reading from the stream.
     * @throws JSONException This is thrown if there are any problems parsing the contents.
     */
    public static JSONObject readAllJsonObject(InputStream inputStream) throws IOException, JSONException {
        return new JSONObject(readAllString(inputStream));
    }

    /**
     * This reads an entire input stream a parses a JSON with the contents.
     *
     * @param inputStream This is the input stream to read until completion. For best performance consider passing an InputStream that has been wrapped in a BufferedInputStream.
     * @return This is the JSON array after parsing.
     * @throws IOException   This is thrown if there are any problems reading from the stream.
     * @throws JSONException This is thrown if there are any problems parsing the contents.
     */
    public static JSONArray readAllJsonArray(InputStream inputStream) throws IOException, JSONException {
        return new JSONArray(readAllString(inputStream));
    }

    private static void copy(InputStream inputStream, OutputStream outputStream) throws IOException {
        while (true) {
            // Read the data
            int data = inputStream.read();

            // Check if the data in less than zero indicating the read is finished
            if (data < 0) {
                break;
            }

            // Write the data to the output stream
            outputStream.write(data);
        }
    }
}
