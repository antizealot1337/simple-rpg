package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.ElementalType;
import me.cybersensei.simplerpg.lib.JSONSerializable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for ElementType.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.ElementalType
 */
public class ElementalTypeTest {
    // The element name
    private static final String NAME = "New Element";

    // This is the element for testing
    private ElementalType element;

    @Before
    public void setUp() {
        this.element = new ElementalType(NAME);
    }

    @Test
    public void testInitialValues() {
        Assert.assertEquals("Checking name", NAME, this.element.getName());
        Assert.assertNotNull("Checking strengths exists", this.element.getStrengths());
        Assert.assertNotNull("Checking weaknesses exists", this.element.getWeaknesses());
    }

    @Test
    public void testFromJSON() throws JSONException {
        final String NAME = "Fire";
        final String STRONG_AGAINST = "Plant";
        final String WEAK_AGAINST = "Water";

        this.element.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(JSONSerializable.STRENGTHS, new JSONArray().put(STRONG_AGAINST))
                .put(JSONSerializable.WEAKNESSES, new JSONArray().put(WEAK_AGAINST))
        );

        Assert.assertEquals("Checking name", NAME, this.element.getName());
        Assert.assertEquals("Checking the number of strengths", 1, this.element.getStrengths().size());
        Assert.assertEquals("Checking the first strength", STRONG_AGAINST, this.element.getStrengths().get(0));
        Assert.assertEquals("Checking the number of weaknesses", 1, this.element.getWeaknesses().size());
        Assert.assertEquals("Checking the first weakness", WEAK_AGAINST, this.element.getWeaknesses().get(0));
    }

    @Test
    public void testToJSON() throws JSONException {
        this.element.setName("Fire");
        this.element.addStrength("Plant");
        this.element.addWeakness("Water");
        JSONObject jSONObject = this.element.toJSON();
        Assert.assertEquals("Checking name", this.element.getName(),
                jSONObject.getString(JSONSerializable.NAME));
        Assert.assertEquals("Checking for strength", this.element.getStrengths().get(0),
                jSONObject.getJSONArray(JSONSerializable.STRENGTHS).getString(0));
        Assert.assertEquals("Checking for weakness", this.element.getWeaknesses().get(0),
                jSONObject.getJSONArray(JSONSerializable.WEAKNESSES).getString(0));
    }

    @Test
    public void testStrength() {
        Assert.assertTrue("Checking was added", this.element.addStrength("Dark"));
        Assert.assertEquals("Checking strengths size", 1, this.element.getStrengths().size());
        Assert.assertFalse("Checking was not added", this.element.addStrength("Dark"));
        Assert.assertTrue("Checking is strong against", this.element.isStrongAgainst("Dark"));
    }

    @Test
    public void testWeakness() {
        Assert.assertTrue("Checking was added", this.element.addWeakness("Fighting"));
        Assert.assertEquals("Checking weaknesses size", 1, this.element.getWeaknesses().size());
        Assert.assertFalse("Checking was not added", this.element.addWeakness("Fighting"));
        Assert.assertTrue("Checking is weak against", this.element.isWeakAgainst("Fighting"));
    }

    @Test
    public void testCalculateDamageMultiplier() {
        final ElementalType STRONG = new ElementalType("Strong");
        final ElementalType WEAK = new ElementalType("Weak");

        this.element.addStrength(STRONG.getName());
        this.element.addWeakness(WEAK.getName());

        Assert.assertEquals("Checking the calculated value against STRONG", 1.5,
                this.element.calculateDamageMultiplier(STRONG), 0.1);
        Assert.assertEquals("Checking the calculated damage against WEAK", 0.75,
                this.element.calculateDamageMultiplier(WEAK), 0.1);
        Assert.assertEquals("Checking the calculated damage against self", 1.0,
                this.element.calculateDamageMultiplier(this.element), 0.1);
    }
}
