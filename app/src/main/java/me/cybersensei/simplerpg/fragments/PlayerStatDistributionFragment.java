package me.cybersensei.simplerpg.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import me.cybersensei.simplerpg.BuildConfig;
import me.cybersensei.simplerpg.R;
import me.cybersensei.simplerpg.lib.Player;

import java.util.Locale;

/**
 * This fragment shows the players stats. If there are stat points available it also shows the controls that allow the
 * points to be distributed. Any Activity that houses this fragment should implement the StatChangedListener.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.fragments.PlayerStatDistributionFragment.StatChangedListener
 */
public class PlayerStatDistributionFragment extends Fragment implements View.OnClickListener {
    // The tag for logging
    private static final String TAG = PlayerStatDistributionFragment.class.getSimpleName();

    // This is the listener for this fragment
    private StatChangedListener listener;

    // These are tags for storing and retrieving the values on state change to the
    // fragment
    private static final String STAT_POINTS = "stat points";
    private static final String CONSTITUTION = "max hp";
    private static final String MIN_CONSTITUTION = "minimum health";
    private static final String STRENGTH = "attack";
    private static final String MIN_STRENGTH = "minimum attack";
    private static final String DEXTERITY = "defense";
    private static final String MIN_DEXTERITY = "minimum defense";
    private static final String AGILITY = "speed";
    private static final String MIN_AGILITY = "minimum speed";
    private static final String DAMAGE = "damage";
    private static final String MIN_DAMAGE = "minimum damage";

    // The views related to strength
    private TextView strength;
    private Button strengthDec;
    private Button strengthInc;

    // The views related to dexterity
    private TextView dexterity;
    private Button dexterityDec;
    private Button dexterityInc;

    // The views related to constitution
    private TextView constitution;
    private Button constitutionDec;
    private Button constitutionInc;

    // The views related to agility
    private TextView agility;
    private Button agilityDec;
    private Button agilityInc;

    // The views related to damage
    private TextView damage;
    private Button damageDec;
    private Button damageInc;

    // The available stat points
    private TextView statPoints;

    // These are the current/minimum values of the player's stats
    private int minStrength;
    private int minDexterity;
    private int minConstitution;
    private int minAgility;
    private int minDamage;

    // These are the temporary/future values of the player's stats
    private int tempStrength;
    private int tempDexterity;
    private int tempConstitution;
    private int tempAgility;
    private int tempDamage;
    private int tempStatPoints;

    @Override
    public void onAttach(Context context) {
        // Cast the attached context to the StatChangedListener
        this.listener = (StatChangedListener) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        // Inflate the layout
        View view = layoutInflater.inflate(R.layout.fragment_player_stat_distribution, viewGroup, false);

        // Find specific children
        this.statPoints = view.findViewById(R.id.tv_stat_points_value);
        this.constitution = view.findViewById(R.id.tv_constitution_value);
        this.strength = view.findViewById(R.id.tv_strength_value);
        this.dexterity = view.findViewById(R.id.tv_dexterity_value);
        this.agility = view.findViewById(R.id.tv_agility_value);
        this.damage = view.findViewById(R.id.tv_damage_value);
        this.constitutionDec = view.findViewById(R.id.b_constitution_decrease);
        this.constitutionInc = view.findViewById(R.id.b_constitution_increase);
        this.strengthDec = view.findViewById(R.id.b_strength_decrease);
        this.strengthInc = view.findViewById(R.id.b_strength_increase);
        this.dexterityDec = view.findViewById(R.id.b_dexterity_decrease);
        this.dexterityInc = view.findViewById(R.id.b_dexterity_increase);
        this.agilityDec = view.findViewById(R.id.b_agility_decrease);
        this.agilityInc = view.findViewById(R.id.b_agility_increase);
        this.damageDec = view.findViewById(R.id.b_damage_decrease);
        this.damageInc = view.findViewById(R.id.b_damage_increase);

        // Set listener
        this.constitutionDec.setOnClickListener(this);
        this.constitutionInc.setOnClickListener(this);
        this.strengthDec.setOnClickListener(this);
        this.strengthInc.setOnClickListener(this);
        this.dexterityDec.setOnClickListener(this);
        this.dexterityInc.setOnClickListener(this);
        this.agilityDec.setOnClickListener(this);
        this.agilityInc.setOnClickListener(this);
        this.damageDec.setOnClickListener(this);
        this.damageInc.setOnClickListener(this);

        // Check if there is bundled state
        if (bundle != null) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Bundled state found");
            loadState(bundle);
        }
        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        if (BuildConfig.DEBUG) Log.d(TAG, "Saving state");
        bundle.putInt(STAT_POINTS, this.tempStatPoints);
        bundle.putInt(CONSTITUTION, this.tempConstitution);
        bundle.putInt(MIN_CONSTITUTION, this.minConstitution);
        bundle.putInt(STRENGTH, this.tempStrength);
        bundle.putInt(MIN_STRENGTH, this.minStrength);
        bundle.putInt(DEXTERITY, this.tempDexterity);
        bundle.putInt(MIN_DEXTERITY, this.minDexterity);
        bundle.putInt(AGILITY, this.tempAgility);
        bundle.putInt(MIN_AGILITY, this.minAgility);
        bundle.putInt(DAMAGE, this.tempDamage);
        bundle.putInt(MIN_DAMAGE, this.minDamage);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null;
    }

    @Override
    public void onClick(View view) {
        // Determine what to do based on the view id
        switch (view.getId()) {
            case R.id.b_constitution_decrease:
                if (this.tempConstitution > this.minConstitution) {
                    this.tempConstitution--;
                    this.tempStatPoints++;
                }
                break;
            case R.id.b_constitution_increase:
                if (arePointsLeft()) {
                    this.tempConstitution++;
                    this.tempStatPoints--;
                }
                break;
            case R.id.b_strength_decrease:
                if (this.tempStrength > this.minStrength) {
                    this.tempStrength--;
                    this.tempStatPoints++;
                }
                break;
            case R.id.b_strength_increase:
                if (arePointsLeft()) {
                    this.tempStrength++;
                    this.tempStatPoints--;
                }
                break;
            case R.id.b_dexterity_decrease:
                if (this.tempDexterity > this.minDexterity) {
                    this.tempDexterity--;
                    this.tempStatPoints++;
                }
                break;
            case R.id.b_dexterity_increase:
                if (arePointsLeft()) {
                    this.tempDexterity++;
                    this.tempStatPoints--;
                }
                break;
            case R.id.b_agility_decrease:
                if (this.tempAgility > this.minAgility) {
                    this.tempAgility--;
                    this.tempStatPoints++;
                }
                break;
            case R.id.b_agility_increase:
                if (arePointsLeft()) {
                    this.tempAgility++;
                    this.tempStatPoints--;
                }
                break;
            case R.id.b_damage_decrease:
                if (this.tempDamage > this.minDamage) {
                    this.tempDamage--;
                    this.tempStatPoints++;
                }
                break;
            case R.id.b_damage_increase:
                if (arePointsLeft()) {
                    this.tempDamage++;
                    this.tempStatPoints--;
                }
                break;
            default:
                break;
        }

        // Update the text view
        updateTextViews();

        // Update the listener
        this.listener.statsUpdated();
    }

    /**
     * Check if there are points left.
     *
     * @return Returns true if there are unspent stat points.
     */
    public boolean arePointsLeft() {
        return (this.tempStatPoints > 0);
    }

    /**
     * This is called once and early to provide the fragment with the stat values.
     *
     * @param player The player to show stats for.
     * @see me.cybersensei.simplerpg.lib.Player
     */
    public void setStatsFromPlayer(Player player) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Updating status from " + player);

        // Get the stat points
        this.tempStatPoints = player.getStatPoints();

        // Get the strength
        int value = player.getStrength();
        this.tempStrength = value;
        this.minStrength = value;

        // Get the dexterity
        value = player.getDexterity();
        this.tempDexterity = value;
        this.minDexterity = value;

        // Get the constitution
        value = player.getConstitution();
        this.tempConstitution = value;
        this.minConstitution = value;

        // Get the agility
        value = player.getAgility();
        this.tempAgility = value;
        this.minAgility = value;

        // Get the damage
        value = player.getDamage();
        this.tempDamage = value;
        this.minDamage = value;

        // Update the text view with the current values
        updateTextViews();

        // Perform a check to see if the buttons should be set to invisible
        if (this.tempStatPoints == 0)
            hideButtons();
    }

    /**
     * Update the player's stats based on how they were distributed with this
     * fragment.
     *
     * @param player The player to update stats for.
     * @see me.cybersensei.simplerpg.lib.Player
     */
    public void applyStatsToPlayer(Player player) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Applying stats to player");
        player.setStatPoints(this.tempStatPoints);
        player.setStrength(this.tempStrength);
        player.setDexterity(this.tempDexterity);
        player.setConstitution(this.tempConstitution);
        player.setAgility(this.tempAgility);
        player.setDamage(this.tempDamage);
    }

    /**
     * Load the state stored in the bundle.
     *
     * @param bundle This is the bundle containing the state.
     */
    private void loadState(Bundle bundle) {
        if (!bundle.containsKey(STAT_POINTS)) {
            return;
        }

        if (BuildConfig.DEBUG) Log.d(TAG, "Loading state from bundle");

        // Update the values from the bundle
        this.tempStatPoints = bundle.getInt(STAT_POINTS);
        this.tempConstitution = bundle.getInt(CONSTITUTION);
        this.minConstitution = bundle.getInt(MIN_CONSTITUTION);
        this.tempStrength = bundle.getInt(STRENGTH);
        this.minStrength = bundle.getInt(MIN_STRENGTH);
        this.tempDexterity = bundle.getInt(DEXTERITY);
        this.minDexterity = bundle.getInt(MIN_DEXTERITY);
        this.tempAgility = bundle.getInt(AGILITY);
        this.minAgility = bundle.getInt(MIN_AGILITY);
        this.tempDexterity = bundle.getInt(DAMAGE);
        this.minDamage = bundle.getInt(MIN_DAMAGE);

        // Update the text views
        updateTextViews();
    }

    /**
     * Update all the text views that contain either a stat value to the stat points value with their new values.
     */
    private void updateTextViews() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Updating TextViews");
        updateTextView(this.statPoints, this.tempStatPoints);
        updateTextView(this.constitution, this.tempConstitution);
        updateTextView(this.strength, this.tempStrength);
        updateTextView(this.dexterity, this.tempDexterity);
        updateTextView(this.agility, this.tempAgility);
        updateTextView(this.damage, this.tempDamage);
    }

    /**
     * Set the buttons to be the visibility provided. This includes all the buttons that increment or decrement stat
     * values.
     *
     * @see android.view.View
     */
    private void hideButtons() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Hiding buttons");
        this.constitutionDec.setVisibility(View.INVISIBLE);
        this.constitutionInc.setVisibility(View.INVISIBLE);
        this.strengthDec.setVisibility(View.INVISIBLE);
        this.strengthInc.setVisibility(View.INVISIBLE);
        this.dexterityDec.setVisibility(View.INVISIBLE);
        this.dexterityInc.setVisibility(View.INVISIBLE);
        this.agilityDec.setVisibility(View.INVISIBLE);
        this.agilityInc.setVisibility(View.INVISIBLE);
        this.damageDec.setVisibility(View.INVISIBLE);
        this.damageInc.setVisibility(View.INVISIBLE);
    }

    /**
     * Update the text view with the integer value provided.
     *
     * @param textView The TextView to set text value for.
     * @param value    The int to turn into a String for the textView.
     */
    private void updateTextView(TextView textView, int value) {
        textView.setText(String.format(Locale.getDefault(), "%d", value));
    }

    /**
     * This interface is for listening for stat changes.
     */
    public interface StatChangedListener {
        /**
         * This is called when the a stat is modified.
         */
        void statsUpdated();
    }
}
