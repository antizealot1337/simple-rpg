package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.Game;

/**
 * This class contains static data for the Game.
 *
 * @author Christopher Pridgen
 */
public class Static {
    /**
     * This contains the name of the saved game file.
     */
    public static final String CURRENT_GAME_FILE_NAME = "game.json";

    /**
     * This is the static game data.
     */
    public static Game game;
}
