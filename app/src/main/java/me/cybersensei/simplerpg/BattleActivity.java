package me.cybersensei.simplerpg;

import android.app.Activity;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import me.cybersensei.simplerpg.fragments.BattleBeingDisplayFragment;
import me.cybersensei.simplerpg.fragments.BattleControlFragment;
import me.cybersensei.simplerpg.fragments.BattleResultDisplayFragment;
import me.cybersensei.simplerpg.lib.BattleBeing;
import me.cybersensei.simplerpg.lib.BattleResult;
import me.cybersensei.simplerpg.lib.BattleStatic;

/**
 * This Activity is shown when the player is battling an opponent. This Activity expects the Intent to have the name of
 * the opponent to be provided for lookup. It should be provided using the CREATURE_NAME key. At the end of the battle
 * it will either start the VictoryActivity or the DefeatActivity.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.VictoryActivity
 * @see me.cybersensei.simplerpg.DefeatActivity
 */
public class BattleActivity extends Activity implements BattleControlFragment.BattleControlListener {
    // The tag for logging
    private static final String TAG = BattleActivity.class.getSimpleName();

    /**
     * The key for providing the opponent creature's name to the BattleActivity
     */
    public static final String CREATURE_NAME = "creature name";

    // The player key for the bundle
    private static final String PLAYER = "player";

    // The player key for the opponent
    private static final String OPPONENT = "opponent";

    // The player display fragment
    private BattleBeingDisplayFragment playerDisplay;

    // The opponent display fragment
    private BattleBeingDisplayFragment opponentDisplay;

    // The fragment that controls the battle
    private BattleControlFragment control;

    // The fragment that displays things from the battle
    private BattleResultDisplayFragment display;

    // The opponent
    private BattleBeing opponent;

    // The player
    private BattleBeing player;

    @Override
    protected void onCreate(Bundle bundle) {
        // Initialize the activity
        super.onCreate(bundle);
        setContentView(R.layout.activity_battle);

        // Set the player
        if (bundle != null && bundle.containsKey(PLAYER)) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Loading player from bundle");
            this.player = (BattleBeing) bundle.getSerializable(PLAYER);
        } else {
            if (BuildConfig.DEBUG) Log.d(TAG, "Getting player from game");
            this.player = Static.game.getPlayer();
        }
        assert (this.player != null);

        if (BuildConfig.DEBUG) Log.d(TAG, this.player.toString());

        // Find the opponent
        if (bundle != null && bundle.containsKey(OPPONENT)) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Loading opponent from the bundle");
            this.opponent = (BattleBeing) bundle.getSerializable(OPPONENT);
        } else {
            if (BuildConfig.DEBUG) Log.d(TAG, "Getting opponent from game");
            this.opponent = Static.game.findCreatureByName(getIntent().getStringExtra(CREATURE_NAME));

            // Make sure the opponent is fully healed and ready to battle
            this.opponent.fullHeal();
        }
        assert (this.opponent != null);

        if (BuildConfig.DEBUG) Log.d(TAG, this.opponent.toString());

        // Get the FragmentManager
        FragmentManager fragmentManager = getFragmentManager();

        // Get the control fragment
        this.control = (BattleControlFragment) fragmentManager.findFragmentById(R.id.f_battle_control);

        // Get and update the player display
        this.playerDisplay = (BattleBeingDisplayFragment) fragmentManager.findFragmentById(R.id.f_player_info);
        this.playerDisplay.setName(this.player.getName());
        this.playerDisplay.updateHealth(this.player.getHp(), this.player.getMaxHp());

        // Get and update the opponent display
        this.opponentDisplay = (BattleBeingDisplayFragment) fragmentManager.findFragmentById(R.id.f_opponent_info);
        this.opponentDisplay.setName(this.opponent.getName());
        this.opponentDisplay.updateHealth(this.opponent.getHp(), this.opponent.getMaxHp());

        // Get the display and initialize its state
        this.display = (BattleResultDisplayFragment) fragmentManager.findFragmentById(R.id.f_battle_display);
        this.display.encountered(this.opponent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (BuildConfig.DEBUG) Log.d(TAG, "Saving player and opponent state");
        outState.putSerializable(PLAYER, this.player);
        outState.putSerializable(OPPONENT, this.opponent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.battle, menu);
        return true;
    }

    @Override
    public void afterDefeat() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Player lost");
        DefeatActivity.startDefeatActivity(this);
        finish();
    }

    @Override
    public void afterFlee() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Someone fled");
        finish();
    }

    @Override
    public void afterWin() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Player won");

        VictoryActivity.startVictoryActivity(this, this.opponent.getGold(), this.opponent.getExp());
        finish();
    }

    @Override
    public void attack() {
        if (BuildConfig.DEBUG) Log.d(TAG, "player selected attack");
        execute(BattleResult.Action.ATTACK);
    }

    @Override
    public void flee() {
        if (BuildConfig.DEBUG) Log.d(TAG, "player selected flee");
        execute(BattleResult.Action.FLEE);
    }

    /**
     * Execute a battle turn with the provide player action.
     *
     * @param action This is the action the player requested.
     */
    private void execute(BattleResult.Action action) {
        // Disable the control fragment's buttons to prevent accidental clicks
        this.control.setButtonsEnabled(false);

        // Determine if the player or opponent goes first
        if (this.player.getSpeed() > this.opponent.getSpeed()) {
            // Execute the player turn and check if the player attempted to flee
            if (fled(playerTurn(action)))
                // Player has fled
                return;

            // Check if the opponent has been defeated
            if (!defeated(this.opponent)) {
                // Execute the enemy turn and check if the opponent attempted to flee
                fled(enemyTurn());

                // Check if the player was defeated to set the battle control mode
                if (defeated(this.player)) {
                    this.control.setMode(BattleControlFragment.Mode.DEFEATED);
                }
            } else {
                this.control.setMode(BattleControlFragment.Mode.WON);
            }
        } else if (!fled(enemyTurn())) { // Execute the opponent turn first
            // Check if the player was defeated
            if (!defeated(this.player)) {
                // Execute the player's turn and check if the player fled
                fled(playerTurn(action));

                // Check if the opponent was defeated to update the battle mode
                if (defeated(this.opponent)) {
                    this.control.setMode(BattleControlFragment.Mode.WON);
                }
            } else {
                this.control.setMode(BattleControlFragment.Mode.DEFEATED);
            }
        } else {
            return;
        }

        // Update the display fragment
        this.playerDisplay.updateHealth(this.player.getHp(), this.player.getMaxHp());
        this.opponentDisplay.updateHealth(this.opponent.getHp(), this.opponent.getMaxHp());

        // Re-enable the control fragments buttons for next turn
        this.control.setButtonsEnabled(true);
    }

    /**
     * This execute the player's part of the battle turn. It is responsible for acting as the player requested. It only
     * acts on Action.FLEE and Action.ATTACK. Any other actions are invalid.
     *
     * @param action The player requested action.
     * @return The BattleResult contains information regarding what happened regarding the player turn.
     * @see me.cybersensei.simplerpg.lib.BattleResult.Action
     * @see me.cybersensei.simplerpg.lib.BattleResult
     */
    private BattleResult playerTurn(BattleResult.Action action) {
        BattleResult result;
        switch (action) {
            case ATTACK:
                // Player wants to attack
                result = BattleStatic.attack(this.player, this.opponent,
                        BattleStatic.calculateElementMultiplier(
                                Static.game.findElementByName(this.opponent.getElement()),
                                Static.game.findElementByName(this.player.getElement())));
                break;
            case FLEE:
                // Player wants to flee
                result = BattleStatic.flee(this.player, this.opponent);
                break;
            default:
                throw new RuntimeException("Unhandled Action");
        }

        this.display.addResult(result);

        return result;
    }

    /**
     * This checks if the there was a successful flee attempt made. If so it will set the control fragment to the
     * appropriate mode of Mode.PLAYER_FLED or MODE.MONSTER_FLED.
     *
     * @param result The result of some turn.
     * @return Returns true if there was a successful flee attempt made.
     * @see me.cybersensei.simplerpg.lib.BattleResult
     * @see me.cybersensei.simplerpg.fragments.BattleControlFragment
     */
    private boolean fled(BattleResult result) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Checking for flee ACTION");

        // Check if the action was to flee
        if (result.getAction() == BattleResult.Action.FLEE) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Checking if flee successful");

            // Check if the result of the flee was a success
            if (result.isSuccess()) {
                if (BuildConfig.DEBUG) Log.d(TAG, "A flee occurred");

                // Check if it was the player who fled
                if (result.getOrigin().equals(this.player.getName())) {
                    this.control.setMode(BattleControlFragment.Mode.PLAYER_FLED);
                    return true;
                }

                // The opponent fled
                this.control.setMode(BattleControlFragment.Mode.MONSTER_FLED);
                return true;
            }
        }
        return false;
    }

    /**
     * This checks if there was a defeat. If so then the display fragment is notified of the defeat.
     *
     * @param being The BattleBeing to check for defeat.
     * @return This returns true if there was a defeat.
     * @see me.cybersensei.simplerpg.lib.BattleBeing
     * @see BattleResultDisplayFragment
     */
    private boolean defeated(BattleBeing being) {
        if (BuildConfig.DEBUG) {
            Log.d(TAG, "Checking if " + being.getName() + " is defeated");
        }
        BattleResult battleResult = BattleStatic.defeated(being);
        if (battleResult.isSuccess()) {
            this.display.addResult(battleResult);
        }
        return battleResult.isSuccess();
    }

    /**
     * This is the enemy turn part of a battle turn. It determines what the enemy wants to do and executes it.
     *
     * @return This returns the result of the enemy turn.
     * @see me.cybersensei.simplerpg.lib.BattleResult
     */
    private BattleResult enemyTurn() {
        if (BuildConfig.DEBUG) Log.d(TAG, "enemy turn");

        BattleResult battleResult = BattleStatic.attack(this.opponent, this.player,
                BattleStatic.calculateElementMultiplier(
                        Static.game.findElementByName(this.opponent.getElement()),
                        Static.game.findElementByName(this.player.getElement()))
        );

        this.display.addResult(battleResult);
        return battleResult;
    }

    /**
     * This is the preferred was to start a BattleActivity. It creates the Intent required and fills it with the needed
     * values to successfully execute a battle.
     *
     * @param context      This is the parent context.
     * @param creatureName This is the creature name to fight.
     */
    public static void startBattleActivity(Context context, String creatureName) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Request start BattleActivity to fight " + creatureName);

        // Create the intent
        Intent intent = new Intent(context, BattleActivity.class);

        // Populate the intent with data
        intent.putExtra(CREATURE_NAME, creatureName);

        // Start the activity via the supplied context
        context.startActivity(intent);
    }
}
