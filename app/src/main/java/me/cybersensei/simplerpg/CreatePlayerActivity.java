package me.cybersensei.simplerpg;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import me.cybersensei.simplerpg.fragments.PlayerStatDistributionFragment;
import me.cybersensei.simplerpg.lib.Player;

/**
 * This Activity is responsible for creating a new Player for a new game. It sets some initial values and accepts a name
 * for the player. After the player is successfully created it will proceed to the TownActivity.
 *
 * @author Christopoher Pridgen
 * @see me.cybersensei.simplerpg.TownActivity
 */
public class CreatePlayerActivity extends Activity
        implements View.OnClickListener, PlayerStatDistributionFragment.StatChangedListener {
    // The tag for logging
    private static final String TAG = CreatePlayerActivity.class.getSimpleName();

    // The initial values for a new Player
    private static final String NAME = "";
    private static final String ELEMENT = "Normal";
    private static final int LEVEL = 1;
    private static final int EXPERIENCE = 50;
    private static final int GOLD = 8;
    private static final int STRENGTH = 3;
    private static final int DEXTERITY = 3;
    private static final int CONSTITUTION = 3;
    private static final int AGILITY = 3;
    private static final int DAMAGE = 1;
    private static final int STAT_POINTS = 9;

    // The name EditExt
    private EditText name;

    // The fragment that shows and distributes stats
    private PlayerStatDistributionFragment playerStatsModifier;

    // The player being created
    private final Player player = new Player(NAME, ELEMENT, LEVEL, EXPERIENCE, GOLD, STRENGTH, DEXTERITY, CONSTITUTION,
            AGILITY, DAMAGE, STAT_POINTS);

    @Override
    protected void onCreate(Bundle bundle) {
        // Initialize the activity
        super.onCreate(bundle);
        setContentView(R.layout.activity_create_player);

        // Find the name EditExt
        this.name = findViewById(R.id.et_name_entry);

        // Get the player stat distribution fragment and initialize it to the player
        this.playerStatsModifier = (PlayerStatDistributionFragment)
                getFragmentManager().findFragmentById(R.id.f_stats);
        this.playerStatsModifier.setStatsFromPlayer(this.player);

        // Set listeners
        findViewById(R.id.b_create_player).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.create_player, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_create_player:
                // Check for problems
                if (getProblems() == null) {
                    // No problems detected so start the TownActivity
                    completePlayerAndStartGame();
                    return;
                }
                // Problems were detected so inform the player but allow them to
                // continue
                displayConfirmation(getProblems());
                break;
            default:
                break;
        }
    }

    @Override
    public void statsUpdated() {
    }

    /**
     * This displays a confirmation for problems creating a new Player. The problems are not critical. The dialog just
     * wishes to inform the player that there are things left unfinished.
     *
     * @param message This is the message to display to the player/user.
     */
    private void displayConfirmation(String message) {
        // Build and show an AlertDialog
        new AlertDialog.Builder(this)
                .setTitle(R.string.problems_creating_player)
                .setMessage(message)
                .setNegativeButton(R.string.no, null)
                .setCancelable(true)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int button) {
                        CreatePlayerActivity.this.completePlayerAndStartGame();
                    }
                })
                .show();
    }

    /**
     * This returns the non critical problems with continuing with the player creation. These problems include the name
     * being too short (empty), unspent stat points, or both.
     *
     * @return This string explains the problems with character creation. If there are no problems then null is
     * returned.
     */
    private String getProblems() {
        Resources resources = getResources();
        int nameLen = this.name.getText().length();
        boolean statPointsLeft = this.playerStatsModifier.arePointsLeft();

        if (nameLen == 0 && statPointsLeft) {
            return resources.getString(R.string.er_name_and_stat_points);
        }

        if (nameLen == 0) {
            return resources.getString(R.string.er_name_short);
        }

        if (statPointsLeft) {
            return resources.getString(R.string.er_stat_points_left);
        }

        return null;
    }

    /**
     * This updates the Player with the name and stats from creation and starts the TownActivity.
     *
     * @see me.cybersensei.simplerpg.TownActivity
     */
    private void completePlayerAndStartGame() {
        updatePlayer();

        if (BuildConfig.DEBUG) Log.d(TAG, "Going to starting town");

        // Start playing the game
        TownActivity.startTownActivity(this);
        finish();
    }

    /**
     * This updates the Player with the name and stats from the PlayerStatDistributionFragment and name EditText view.
     */
    private void updatePlayer() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Updating/creating the player");

        // Set the player's name
        this.player.setName(this.name.getText().toString());

        // Update the stats from the fragment
        this.playerStatsModifier.applyStatsToPlayer(this.player);

        // Make sure the player's health is full
        this.player.fullHeal();

        // Set this the the player for the game
        Static.game.setPlayer(this.player);
    }

    /**
     * This is the preferred was to start a CreatePlayerActivity. It creates the Intent required and fills it with the
     * needed values to successfully execute a battle.
     *
     * @param context This is the parent context.
     */
    public static void startCreatePlayerActivity(Context context) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Request start CreatePlayerActivity");

        context.startActivity(new Intent(context, CreatePlayerActivity.class));
    }
}
