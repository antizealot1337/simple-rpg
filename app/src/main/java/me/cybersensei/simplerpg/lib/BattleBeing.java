package me.cybersensei.simplerpg.lib;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.Locale;

/**
 * This is the primary class used for battling. Anything that wants to battle must be a child of this class.
 *
 * @author Christopher Pridgen
 */
public class BattleBeing extends GameObject implements Serializable {
    /**
     * This is the field name for element types.
     */
    public static final String ELEMENTAL_TYPE = "elementalType";

    /**
     * This is the legacy value of ELEMENTAL_TYPE.
     */
    public static final String ELEMENTAL_TYPE_LEGACY = "elemental element";

    /**
     * This is the field name for level.
     */
    public static final String LEVEL = "level";

    /**
     * This is the field name for hp.
     */
    public static final String HP = "hp";

    /**
     * This is the field name for exp.
     */
    public static final String EXP = "exp";

    /**
     * This is the field name for gold.
     */
    public static final String GOLD = "gold";

    /**
     * This is the field name for damage.
     */
    public static final String DAMAGE = "damage";

    /**
     * This is the field name for strength.
     */
    public static final String STRENGTH = "strength";

    /**
     * This is the field name for dexterity.
     */
    public static final String DEXTERITY = "dexterity";

    /**
     * This is the field name for constitution.
     */
    public static final String CONSTITUTION = "constitution";

    /**
     * This is the field name for agility.
     */
    public static final String AGILITY = "agility";

    /**
     * This is the field name for max hp. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    public static final String MAX_HP = "max hp";

    /**
     * This is the field name for attack. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    public static final String ATTACK = "attack";

    /**
     * This is the field name for defense. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    public static final String DEFENSE = "defense";

    /**
     * This is the field name for speed. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    public static final String SPEED = "speed";

    /**
     * This is the name of the element. It is used to calculate elemental advantages.
     */
    protected String element;

    /**
     * This is the current hp.
     */
    protected int hp;

    /**
     * This is the current level.
     */
    protected int level;

    /**
     * This is the current experience.
     */
    protected int exp;

    /**
     * This is the strength. It factors into attack ratings.
     */
    protected int strength;

    /**
     * This is the dexterity. It factors into defense ratings.
     */
    protected int dexterity;

    /**
     * This is the constitution. It factors into the max health.
     */
    protected int constitution;

    /**
     * This is the agility. It factors into speed.
     */
    protected int agility;

    /**
     * This is the damage. It is a raw value of how much damage is done for a successful attack.
     */
    protected int damage;

    /**
     * This is the current gold.
     */
    protected int gold;

    /**
     * This creates a BattleBeing with default values. The name is "Battle Being" and element is "Normal". The level,
     * experience, gold, strength, dexterity, constitution, agility, and damage are all set to one. This method of
     * creating a BattleBeing is mostly used in situation where the actual values will be set later.
     */
    public BattleBeing() {
        this("Battle Being", "Normal", 1, 1, 1, 1, 1, 1, 1, 1);
    }

    /**
     * This is the method to create a BattleBeing when all the values are known at the time of construction.
     *
     * @param name         This is the name.
     * @param element      This is the element name.
     * @param level        This is the level.
     * @param experience   This is the experience.
     * @param gold         This is the gold.
     * @param strength     This is the strength.
     * @param dexterity    This is the dexterity.
     * @param constitution This is the constitution.
     * @param agility      This is the agility.
     * @param damage       This is the damage.
     */
    public BattleBeing(String name, String element, int level, int experience, int gold, int strength, int dexterity, int constitution, int agility, int damage) {
        this.name = name;
        this.element = element;
        this.level = level;
        this.exp = experience;
        this.gold = gold;
        this.strength = strength;
        this.dexterity = dexterity;
        this.constitution = constitution;
        this.agility = agility;
        this.damage = damage;
    }

    @Override
    public String toString() {
        return String.format(Locale.US,
                "%s type:%s level:%d exp:%d gold:%d strength:%d dexterity:%d constitution:%d agility:%d damage:%d",
                this.name, this.element, this.level, this.exp, this.gold, this.strength,
                this.dexterity, this.constitution, this.agility, this.damage);
    }

    /**
     * Load data from a JSONObject into this BattleBeing. There are two loading paths for a BattleBeing. One being a
     * legacy path for early BattleBeing that had stats like max hp, attack, defense, and speed while the other for
     * those that have actual stats like strength, dexterity, constitution, and agility.
     *
     * @param jsonObject The JSONObject with the data to be loaded.
     * @throws JSONException This is thrown if there is an unexpected JSON related exception.
     * @see org.json.JSONObject
     */
    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        // Get values from the JSONObject and set them to this BattleBeing
        super.fromJSON(jsonObject);
        if (jsonObject.has(ELEMENTAL_TYPE)) {
            this.element = jsonObject.getString(ELEMENTAL_TYPE);
        } else if (jsonObject.has(BattleBeing.ELEMENTAL_TYPE_LEGACY)) {
            this.element = jsonObject.getString(ELEMENTAL_TYPE_LEGACY);
        } else {
            this.element = "Normal";
        }
        this.level = jsonObject.getInt(LEVEL);
        this.hp = jsonObject.getInt(HP);
        this.exp = jsonObject.getInt(EXP);
        this.gold = jsonObject.getInt(GOLD);
        this.damage = jsonObject.getInt(DAMAGE);

        // Check if this is data corresponds to newer BattleBeing data
        if (jsonObject.has(STRENGTH)) {
            this.strength = jsonObject.getInt(STRENGTH);
            this.dexterity = jsonObject.getInt(DEXTERITY);
            this.constitution = jsonObject.getInt(CONSTITUTION);
            this.agility = jsonObject.getInt(AGILITY);
            return;
        }

        // This data corresponds to a legacy BattleBeing
        setMaxHp(jsonObject.getInt(MAX_HP));
        setAttack(jsonObject.getInt(ATTACK));
        setDefense(jsonObject.getInt(DEFENSE));
        setSpeed(jsonObject.getInt(SPEED));
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        return super.toJSON()
                .put(ELEMENTAL_TYPE, this.element)
                .put(LEVEL, this.level)
                .put(HP, this.hp)
                .put(EXP, this.exp)
                .put(GOLD, this.gold)
                .put(DAMAGE, this.damage)
                .put(STRENGTH, this.strength)
                .put(DEXTERITY, this.dexterity)
                .put(CONSTITUTION, this.constitution)
                .put(AGILITY, this.agility);
    }

    /**
     * Get the element name
     *
     * @return The String name of the corresponding element.
     * @see me.cybersensei.simplerpg.lib.ElementalType
     */
    public String getElement() {
        return this.element;
    }

    /**
     * Set the element name
     *
     * @param element The String name of the corresponding element.
     * @see me.cybersensei.simplerpg.lib.ElementalType
     */
    public void setElement(String element) {
        this.element = element;
    }

    /**
     * Get the current health points. There is no setter for hp. All health modifications should got through the
     * damage, heal, and fullHeal methods.
     *
     * @return This returns the current value of the health.
     */
    public int getHp() {
        return this.hp;
    }

    /**
     * Get the current level.
     *
     * @return This returns the current level.
     */
    public int getLevel() {
        return this.level;
    }

    /**
     * Set the current level. This is mostly used for loading data and testing.
     *
     * @param level This is the new value for level.
     */
    public void setLevel(int level) {
        this.level = level;
    }

    /**
     * Get the current exp.
     *
     * @return This returns the current exp.
     */
    public int getExp() {
        return this.exp;
    }

    /**
     * Set the current exp.
     *
     * @param exp This is the new value for exp.
     */
    public void setExp(int exp) {
        this.exp = exp;
    }

    /**
     * Get the strength.
     *
     * @return This returns the current strength.
     */
    public int getStrength() {
        return this.strength;
    }

    /**
     * Set the strength.
     *
     * @param strength This is the new value for strength.
     */
    public void setStrength(int strength) {
        this.strength = strength;
    }

    /**
     * Get the dexterity.
     *
     * @return This returns the current dexterity.
     */
    public int getDexterity() {
        return this.dexterity;
    }

    /**
     * Set the dexterity.
     *
     * @param dexterity This is the new value for dexterity.
     */
    public void setDexterity(int dexterity) {
        this.dexterity = dexterity;
    }

    /**
     * Get the constitution.
     *
     * @return This returns the current constitution.
     */
    public int getConstitution() {
        return this.constitution;
    }

    /**
     * Set the constitution.
     *
     * @param constitution This is the new value for constitution.
     */
    public void setConstitution(int constitution) {
        this.constitution = constitution;
    }

    /**
     * Get the agility.
     *
     * @return The returns the current agility.
     */
    public int getAgility() {
        return this.agility;
    }

    /**
     * Set the agility.
     *
     * @param agility This is the new value for agility.
     */
    public void setAgility(int agility) {
        this.agility = agility;
    }

    /**
     * Get the damage.
     *
     * @return This returns the current damage.
     */
    public int getDamage() {
        return this.damage;
    }

    /**
     * Set the damage.
     *
     * @param damage This is the new value for damage.
     */
    public void setDamage(int damage) {
        this.damage = damage;
    }

    /**
     * Get the current gold.
     *
     * @return This returns the current gold amount.
     */
    public int getGold() {
        return this.gold;
    }

    /**
     * Set the current gold.
     *
     * @param gold This is the new amount for gold.
     */
    public void setGold(int gold) {
        this.gold = gold;
    }

    /**
     * Calculate and return the max health points. This is based off of the constitution stat.
     *
     * @return This returns the calculated maximum health points.
     */
    public int getMaxHp() {
        return (int) Math.ceil(this.constitution * 1.25);
    }

    /**
     * Set the maximum health points. This is only used for converting an old BattleBeing to a new BattleBeing.
     *
     * @param maxHp This is the new maximum health points.
     */
    @Deprecated
    public void setMaxHp(int maxHp) {
        this.constitution = Math.round(maxHp * 0.75f);
    }

    /**
     * Calculate and return the attack value. This is based off of the strength stat.
     *
     * @return This returns the calculated attack.
     */
    public int getAttack() {
        return (int) Math.ceil((this.strength * 0.5));
    }

    /**
     * Set the attack. This is only used for converting an old BattleBeing to a new BattleBeing.
     *
     * @param attack This is the new attack.
     */
    @Deprecated
    public void setAttack(int attack) {
        this.strength = attack;
    }

    /**
     * Calculate and return the defense value. This is based off of the dexterity stat.
     *
     * @return This returns the calculated defense.
     */
    public int getDefense() {
        return (int) Math.floor((this.dexterity * 0.75));
    }

    /**
     * Set the defense. This is only used for converting an old BattleBeing to a new BattleBeing.
     *
     * @param amount This is the new defense.
     */
    @Deprecated
    public void setDefense(int amount) {
        this.dexterity = amount;
    }

    /**
     * Calculate and return the speed value. This is based off of the agility stat.
     *
     * @return This returns the calculated speed.
     */
    public int getSpeed() {
        return (int) Math.ceil((this.agility * 2.0 / 3.0));
    }

    /**
     * Set the speed. This is only used for converting an old BattleBeing to a new BattleBeing.
     *
     * @param speed This is the new speed.
     */
    @Deprecated
    public void setSpeed(int speed) {
        this.agility = speed;
    }


    /**
     * This will decrease the hp of the BattleBeing.
     *
     * @param amount This is the amount of damage to apply. If is is less than zero no then the hp is left as is.
     */
    public void damage(int amount) {
        if (amount < 0)
            return;
        this.hp -= amount;
    }

    /**
     * This will increase the hp of the BattleBeing.
     *
     * @param amount This is the amount of heal to apply. If it is less than zero no then the hp is left as is.
     */
    public void heal(int amount) {
        if (amount < 0)
            return;
        if (this.hp + amount > getMaxHp()) {
            fullHeal();
            return;
        }
        this.hp += amount;
    }

    /**
     * This will unconditionally set the current hp to the max health points.
     */
    public void fullHeal() {
        this.hp = getMaxHp();
    }
}
