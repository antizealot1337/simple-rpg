package me.cybersensei.simplerpg.lib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This is all the static data the makes up the world. It contains element data, creature data, and town data.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.ElementalType
 * @see me.cybersensei.simplerpg.lib.Creature
 * @see me.cybersensei.simplerpg.lib.Town
 */
public class WorldData implements JSONSerializable {
    /**
     * This is the field name for elements
     */
    public static final String ELEMENTS = "elements";

    /**
     * This is the field name for creatures.
     */
    public static final String CREATURES = "creatures";

    /**
     * This is the field name for towns.
     */
    public static final String TOWNS = "towns";

    /**
     * This is the field name for the starting town.
     */
    public static final String STARTING_TOWN = "staringTown";

    // The list of elements
    private final ArrayList<ElementalType> elements = new ArrayList<>();

    // The list of creatures
    private final ArrayList<Creature> creatures = new ArrayList<>();

    // The list of towns
    private final ArrayList<Town> towns = new ArrayList<>();

    // This could be the name of the starting town or null
    private String startingTown;

    /**
     * This is the primary constructor. It accepts lists of elements, creatures, and towns that are in the world.
     *
     * @param elements     This is the list of elements.
     * @param creatures    This is the list of creatures.
     * @param towns        This is the list of towns.
     * @param startingTown This is the name of the starting town. This may be null and the first town will be used
     *                     instead.
     */
    public WorldData(List<ElementalType> elements, List<Creature> creatures, List<Town> towns, String startingTown) {
        this.elements.addAll(elements);
        this.creatures.addAll(creatures);
        this.towns.addAll(towns);
        this.startingTown = startingTown;
    }

    /**
     * This constructor will make an object from the supplied JSON data. This calls fromJSON to parse all the WorldData
     * from the JSON data.
     *
     * @param jsonObject This is the JSON data that contains world data.
     * @throws JSONException This will be thrown in case there is a parse error.
     */
    public WorldData(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }

    @Override
    public String toString() {
        return "WorldData{" +
                "elements=" + elements +
                ", creatures=" + creatures +
                ", towns=" + towns +
                '}';
    }

    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        // Check for the starting town
        if (jsonObject.has(STARTING_TOWN)){
            this.startingTown = jsonObject.getString(STARTING_TOWN);
        }

        // Get the elements
        JSONArray elementsArray = jsonObject.getJSONArray(ELEMENTS);
        for (int i = 0; i < elementsArray.length(); i++) {
            this.elements.add(new ElementalType(elementsArray.getJSONObject(i)));
        }

        // Get the creatures
        JSONArray creatureArray = jsonObject.getJSONArray(CREATURES);
        for (int i = 0; i < creatureArray.length(); i++) {
            this.creatures.add(new Creature(creatureArray.getJSONObject(i)));
        }

        // Get the towns
        JSONArray townArray = jsonObject.getJSONArray(TOWNS);
        for (int i = 0; i < townArray.length(); i++) {
            this.towns.add(new Town(townArray.getJSONObject(i)));
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        // Build the elements json array
        JSONArray elementsArray = new JSONArray();
        for (ElementalType element : this.elements) {
            elementsArray.put(element.toJSON());
        }

        // Build the creatures json array
        JSONArray creaturesArray = new JSONArray();
        for (Creature creature : this.creatures) {
            creaturesArray.put(creature.toJSON());
        }

        // Build the towns json array
        JSONArray townsArray = new JSONArray();
        for (Town town : this.towns) {
            townsArray.put(town.toJSON());
        }

        // Build the final json object
        return new JSONObject()
                .put(ELEMENTS, elementsArray)
                .put(CREATURES, creaturesArray)
                .put(TOWNS, townsArray)
                .put(STARTING_TOWN, this.startingTown);
    }

    /**
     * This will return the name of the starting town. If no starting town was explicitly set then it will return the
     * first town in the provided list. It expects there to be at least 1 town available.
     *
     * @return This is the name of the starting town.
     */
    public String getStartingTown() {
        if (this.startingTown == null) {
            return this.towns.get(0).getName();
        }

        return this.startingTown;
    }

    /**
     * This attempts to find an ElementType by using its name.
     *
     * @param name This is the name of the element.
     * @return This will either be the requested element, or null if it wasn't found.
     */
    public ElementalType findElement(String name) {
        for (ElementalType element : this.elements) {
            if (element.getName().equalsIgnoreCase(name)) {
                return element;
            }
        }
        return null;
    }

    /**
     * This attempts to find an Creature by using its name.
     *
     * @param name This is the name of the creature.
     * @return This will either be the requested creature, or null if it wasn't found.
     */
    public Creature findCreature(String name) {
        for (Creature creature : this.creatures) {
            if (creature.getName().equalsIgnoreCase(name)) {
                return creature;
            }
        }
        return null;
    }

    /**
     * This attempts to find an Town by using its name.
     *
     * @param name This is the name of the creature.
     * @return This will either be the requested creature, or null if it wasn't found.
     */
    public Town findTown(String name) {
        for (Town town : this.towns) {
            if (town.getName().equalsIgnoreCase(name)) {
                return town;
            }
        }
        return null;
    }
}
