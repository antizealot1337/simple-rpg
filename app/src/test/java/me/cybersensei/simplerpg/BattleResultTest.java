package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.BattleResult;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for BattleResult.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.BattleResult
 */
public class BattleResultTest {
    // The battle result origin
    private static final String ORIGIN = "Origin";

    // The battle result target
    private static final String TARGET = "Target";

    // The battle result success
    private static final boolean SUCCESS = true;

    // The battle result message
    private static final String MESSAGE = "";

    // This is the battle result for testing
    private BattleResult battleResult;

    @Before
    public void setUp() {
        // This creates a new battle result
        this.battleResult = new BattleResult();
    }

    @Test
    public void testAttack() {
        battleResult.attack(ORIGIN, TARGET, SUCCESS, MESSAGE);

        Assert.assertEquals("Checking the action", BattleResult.Action.ATTACK, battleResult.getAction());
        Assert.assertEquals("Checking the origin", ORIGIN, battleResult.getOrigin());
        Assert.assertEquals("Checking the target", TARGET, battleResult.getTarget());
        Assert.assertEquals("Checking the success", SUCCESS, battleResult.isSuccess());
        Assert.assertEquals("Checking the message", MESSAGE, battleResult.getMessage());
    }

    @Test
    public void testDefeat() {
        battleResult.defeat(ORIGIN, SUCCESS, MESSAGE);

        Assert.assertEquals("Checking the action", BattleResult.Action.VICTORY, battleResult.getAction());
        Assert.assertEquals("Checking the origin", ORIGIN, battleResult.getOrigin());
        Assert.assertNull("Checking the target", battleResult.getTarget());
        Assert.assertEquals("Checking the success", SUCCESS, battleResult.isSuccess());
        Assert.assertEquals("Checking the message", MESSAGE, battleResult.getMessage());
    }

    @Test
    public void testFlee() {
        battleResult.flee(ORIGIN, SUCCESS, MESSAGE);

        Assert.assertEquals("Checking the action", BattleResult.Action.FLEE, battleResult.getAction());
        Assert.assertEquals("Checking the origin", ORIGIN, battleResult.getOrigin());
        Assert.assertNull("Checking the target", battleResult.getTarget());
        Assert.assertEquals("Checking the success", SUCCESS, battleResult.isSuccess());
        Assert.assertEquals("Checking the message", MESSAGE, battleResult.getMessage());
    }

    @Test
    public void testReset() {
        battleResult.attack(ORIGIN, TARGET, SUCCESS, MESSAGE);
        battleResult.reset();

        Assert.assertNull("Checking the action", battleResult.getAction());
        Assert.assertNull("Checking the origin", battleResult.getOrigin());
        Assert.assertNull("Checking the target", battleResult.getTarget());
        Assert.assertFalse("Checking the success", battleResult.isSuccess());
        Assert.assertNull("Checking the message", battleResult.getMessage());
    }
}
