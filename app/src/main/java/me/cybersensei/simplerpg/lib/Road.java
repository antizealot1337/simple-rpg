package me.cybersensei.simplerpg.lib;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * This is a road to another place. Road names usually correspond to the place the road leads.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.Town
 */
public class Road extends GameObject {
    /**
     * This is the field name for the end down.
     */
    public static final String END_TOWN = "destination";

    /**
     * This is the field name to indicate if the road is enabled.
     */
    public static final String ENABLED = "enabled";

    // Controls if the road is enabled or not
    private boolean enabled;

    /**
     * This is the primary constructor.
     *
     * @param name    This is the name of the end point of the road.
     * @param enabled This determines if the road is enabled or not.
     */
    public Road(String name, boolean enabled) {
        this.name = name;
        this.enabled = enabled;
    }

    /**
     * This constructor creates a Road from a JSONObject. It is similar to creating a new object and calling fromJSON
     * but is safer.
     *
     * @param jsonObject This is the JSONObject that holds the Road data.
     * @throws JSONException This is an exception thrown by fromJSON.
     */
    public Road(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }

    @Override
    public String toString() {
        return String.format(Locale.US, "to %s(%b)", this.name, this.enabled);
    }

    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        this.name = jsonObject.getString(END_TOWN);
        this.enabled = jsonObject.getBoolean(ENABLED);
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = new JSONObject();
        jsonObject.put(END_TOWN, this.name);
        jsonObject.put(ENABLED, this.enabled);
        return jsonObject;
    }

    /**
     * Determine if this is enabled or not.
     *
     * @return This will be true if the road is enabled or false if not.
     */
    public boolean getEnabled() {
        return this.enabled;
    }

    /**
     * Set enabled.
     *
     * @param enabled This is the new value for enabled.
     */
    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
