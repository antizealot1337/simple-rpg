package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.JSONSerializable;
import me.cybersensei.simplerpg.lib.Player;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for Player.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.Player
 */
public class PlayerTest {
    // The player name
    private static final String NAME = "Player";

    // The player element
    private static final String ELEMENT = "Normal";

    // The player level
    private static final int LEVEL = 1;

    // The player experience
    private static final int EXPERIENCE = 50;

    // The player gold
    private static final int GOLD = 2;

    // The player strength
    private static final int STRENGTH = 3;

    // The player dexterity
    private static final int DEXTERITY = 4;

    // The player constitution
    private static final int CONSTITUTION = 5;

    // The player agility
    private static final int AGILITY = 6;

    // The player damage
    private static final int DAMAGE = 7;

    // The player stat points
    private static final int STAT_POINTS = 8;

    // This is the player for testing
    private Player player;

    @Before
    public void setUp() {
        this.player = new Player(NAME, ELEMENT, LEVEL, EXPERIENCE, GOLD, STRENGTH, DEXTERITY, CONSTITUTION, AGILITY, DAMAGE, STAT_POINTS);
    }

    @Test
    public void testFromJSON() throws JSONException {
        final int NEW_STAT_POINTS = 20;

        this.player.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(JSONSerializable.ELEMENTAL_TYPE, ELEMENT)
                .put(JSONSerializable.HP, 0)
                .put(JSONSerializable.LEVEL, LEVEL)
                .put(JSONSerializable.EXP, EXPERIENCE)
                .put(JSONSerializable.GOLD, GOLD)
                .put(JSONSerializable.STRENGTH, STRENGTH)
                .put(JSONSerializable.DEXTERITY, DEXTERITY)
                .put(JSONSerializable.CONSTITUTION, CONSTITUTION)
                .put(JSONSerializable.AGILITY, AGILITY)
                .put(JSONSerializable.DAMAGE, DAMAGE)
                .put(JSONSerializable.STAT_POINTS, NEW_STAT_POINTS)
        );
        // Only test the fields that are local to Player. Ignore fields from parent
        Assert.assertEquals("Checking the stat points", NEW_STAT_POINTS, player.getStatPoints());
    }

    @Test
    public void testFromJsonLegacyFields() throws JSONException {
        final int NEW_STAT_POINTS = 20;

        this.player.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(JSONSerializable.ELEMENTAL_TYPE, ELEMENT)
                .put(JSONSerializable.HP, 0)
                .put(JSONSerializable.LEVEL, LEVEL)
                .put(JSONSerializable.EXP, EXPERIENCE)
                .put(JSONSerializable.GOLD, GOLD)
                .put(JSONSerializable.STRENGTH, STRENGTH)
                .put(JSONSerializable.DEXTERITY, DEXTERITY)
                .put(JSONSerializable.CONSTITUTION, CONSTITUTION)
                .put(JSONSerializable.AGILITY, AGILITY)
                .put(JSONSerializable.DAMAGE, DAMAGE)
                .put(Player.STAT_POINTS_LEGACY, NEW_STAT_POINTS)
        );
        // Only test the fields that are local to Player. Ignore fields from parent
        Assert.assertEquals("Checking the stat points", NEW_STAT_POINTS, player.getStatPoints());
    }

    @Test
    public void testToJSON() throws JSONException {
        JSONObject jsonObject = player.toJSON();
        // Only test the fields that are local to Player. Ignore fields from parent
        Assert.assertEquals("Checking the stat points", STAT_POINTS,
                jsonObject.getInt(JSONSerializable.STAT_POINTS));
    }

    @Test
    public void testStatPoints() {
        final int NEW_STAT_POINTS = 20;
        this.player.setStatPoints(NEW_STAT_POINTS);
        Assert.assertEquals("Checking the stat points", NEW_STAT_POINTS, player.getStatPoints());
    }

    @Test
    public void testAddExp() {
        int levelGained = this.player.addExp(10);

        Assert.assertEquals("Checking levels gained", 0, levelGained);
        Assert.assertEquals("Checking that the experience was applied", EXPERIENCE - 10, this.player.getExp());

        levelGained = this.player.addExp(EXPERIENCE);

        Assert.assertEquals("Checking levels gained", 1, levelGained);
        Assert.assertEquals("Checking the player level", 2, this.player.getLevel());
        Assert.assertEquals("Checking the strength", STRENGTH + 1, this.player.getStrength());
        Assert.assertEquals("Checking the constitution", CONSTITUTION + 1, this.player.getConstitution());
        Assert.assertEquals("Checking the dexterity", DEXTERITY + 1, this.player.getDexterity());
        Assert.assertEquals("Checking the agility", AGILITY + 1, this.player.getAgility());
        Assert.assertEquals("Checking the stat points", STAT_POINTS + 3, this.player.getStatPoints());
    }

    @Test
    public void testHasEnoughGold() {
        Assert.assertTrue("Checking can afford amount less than gold", this.player.hasEnoughGold(GOLD - 1));
        Assert.assertTrue("Checking can afford amount equal to gold", this.player.hasEnoughGold(GOLD));
        Assert.assertFalse("Checking cannot afford amount greater than gold", this.player.hasEnoughGold(GOLD + 1));
    }

    @Test
    public void testAddGold() {
        this.player.addGold(-10);
        Assert.assertEquals("Checking to make sure negative amounts are ignored", GOLD, this.player.getGold());

        final int AMOUNT = 10;

        this.player.addGold(AMOUNT);
        Assert.assertEquals("Checking 10 gold is added", GOLD + AMOUNT, this.player.getGold());
    }

    @Test
    public void testChargeGold() {
        final int NEW_GOLD = 50;
        this.player.setGold(NEW_GOLD);

        this.player.chargeGold(-1);
        Assert.assertEquals("Checking gold isn't changed from negative charge", NEW_GOLD, this.player.getGold());

        this.player.chargeGold(NEW_GOLD + 1);
        Assert.assertEquals("Checking gold isn't changed for over charge", NEW_GOLD, this.player.getGold());

        final int AMOUNT = 10;
        this.player.chargeGold(AMOUNT);
        Assert.assertEquals("Checking was charged 10 gold", NEW_GOLD - AMOUNT, this.player.getGold());
    }
}
