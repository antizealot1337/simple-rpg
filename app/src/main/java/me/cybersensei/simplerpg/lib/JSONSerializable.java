package me.cybersensei.simplerpg.lib;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This interface provides common methods for serializing and deserializing and object to and from JSON. It also
 * contains a lot of commonly used constants for JSON object field names/keys.
 *
 * @author Christopher Pridgen
 */
public interface JSONSerializable {
    /**
     * This is the field name for agility.
     */
    String AGILITY = BattleBeing.AGILITY;

    /**
     * This is the field name for attack. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    String ATTACK = BattleBeing.ATTACK;

    /**
     * This is the field name for constitution.
     */
    String CONSTITUTION = BattleBeing.CONSTITUTION;

    /**
     * This is the field name for creatures.
     */
    String CREATURES = WorldData.CREATURES;

    /**
     * This is the field name for the current town.
     */
    String CURRENT_TOWN = SaveData.CURRENT_TOWN;

    /**
     * This is the field name for damage.
     */
    String DAMAGE = BattleBeing.DAMAGE;

    /**
     * This is the field name for defense. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    String DEFENSE = BattleBeing.DEFENSE;

    /**
     * This is the field name for dexterity.
     */
    String DEXTERITY = BattleBeing.DEXTERITY;

    /**
     * This is the field name for element types.
     */
    String ELEMENTAL_TYPE = BattleBeing.ELEMENTAL_TYPE;

    /**
     * This is the field name for elements
     */
    String ELEMENTS = WorldData.ELEMENTS;

    /**
     * This is the field name to indicate if something is enabled.
     */
    String ENABLED = Road.ENABLED;

    /**
     * This is the field name for the end down.
     */
    String END_TOWN = Road.END_TOWN;

    /**
     * This is the field name for exp.
     */
    String EXP = BattleBeing.EXP;

    /**
     * This is the field name for field creatures.
     */
    String FIELD_CREATURES = Town.FIELD_CREATURES;

    /**
     * This is the field name for gold.
     */
    String GOLD = BattleBeing.GOLD;

    /**
     * This is the field name for hp.
     */
    String HP = BattleBeing.HP;

    /**
     * This is the field name for inn cost.
     */
    String INN_COST = Town.INN_COST;

    /**
     * This is the field name for an inn name.
     */
    String INN_NAME = Town.INN_NAME;

    /**
     * This is the field name for level.
     */
    String LEVEL = BattleBeing.LEVEL;

    /**
     * This is the field name for max hp. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    String MAX_HP = BattleBeing.MAX_HP;

    /**
     * This is the field name for name.
     */
    String NAME = GameObject.NAME;

    /**
     * This is the field name for player.
     */
    String PLAYER = SaveData.PLAYER;

    /**
     * This is the field name for roads.
     */
    String ROADS = Town.ROADS;

    /**
     * This is the field name for speed. This field corresponds to an older stat that no longer exists and should not
     * be used.
     */
    @Deprecated
    String SPEED = BattleBeing.SPEED;

    /**
     * This is the field name for the starting town.
     */
    String STARTING_TOWN = WorldData.STARTING_TOWN;

    /**
     * This is the field name for stat points.
     */
    String STAT_POINTS = Player.STAT_POINTS;

    /**
     * This is the field name for strength.
     */
    String STRENGTH = BattleBeing.STRENGTH;

    /**
     * This is the field name for strengths.
     */
    String STRENGTHS = ElementalType.STRENGTHS;

    /**
     * This is the field name for towns.
     */
    String TOWNS = WorldData.TOWNS;

    /**
     * This is the field name for weaknesses.
     */
    String WEAKNESSES = ElementalType.WEAKNESSES;

    /**
     * Load data from a JSONObject into this GameObject.
     *
     * @param jsonObject This is the JSONObject that contains the data to be loaded.
     * @throws JSONException This is thrown if there is an unexpected JSON related exception.
     * @see org.json.JSONObject
     */
    void fromJSON(JSONObject jsonObject) throws JSONException;

    /**
     * Convert this GameObject to a JSONObject.
     *
     * @return This object's data successfully serialized into a JSONObject.
     * @throws JSONException This is thrown if there is an unexpected JSON related exception.
     * @see org.json.JSONObject
     */
    JSONObject toJSON() throws JSONException;
}
