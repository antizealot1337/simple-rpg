package me.cybersensei.simplerpg;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import me.cybersensei.simplerpg.lib.Game;
import me.cybersensei.simplerpg.lib.IOUtils;
import me.cybersensei.simplerpg.lib.SaveData;
import me.cybersensei.simplerpg.lib.WorldData;
import org.json.JSONException;

import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * This is the first activity of the game. It allows the player the option to create a new name of if available to
 * continue an old one.
 *
 * @author Christpoher Pridgen
 */
public class StartGameActivity extends Activity implements View.OnClickListener {
    // This is the name of the world data file
    private static final String WORLD_DATA_FILENAME = "world.json";

    // The tag for logging
    public static final String TAG = StartGameActivity.class.getSimpleName();

    // The game data
    private Game game;

    @Override
    protected void onCreate(Bundle bundle) {
        // Initialize the activity
        super.onCreate(bundle);
        setContentView(R.layout.activity_start_game);

        // Find children views
        Button newGame = findViewById(R.id.b_new_game);
        Button continueGame = findViewById(R.id.b_continue_game);

        // Set listeners
        newGame.setOnClickListener(this);
        continueGame.setOnClickListener(this);

        // This is the world data
        WorldData worldData;

        try {
            // Load the world data
            worldData = loadWorldData();
        } catch (Exception e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
            informLoadWorldDataFailure();
            finish();
            return;
        }

        // This may be saved data
        SaveData saveData = null;

        try {
            saveData = loadSavedData();
        } catch (FileNotFoundException ignored) {

        } catch (IOException e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
        } catch (JSONException e) {
            Log.e(TAG, e.toString());
            e.printStackTrace();
            informGameFileCorrupted();
        }

        // Initialize the game
        this.game = new Game(worldData, saveData);

        // Set the continue button enabled based on the existence of saved data
        continueGame.setEnabled(saveData != null);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.start_game, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_new_game:
                Static.game = this.game;
                if (BuildConfig.DEBUG) Log.d(TAG, Static.game.toString());
                startNewGame();
                break;
            case R.id.b_continue_game:
                Static.game = this.game;
                if (BuildConfig.DEBUG) Log.d(TAG, Static.game.toString());
                continuePreviousGame();
                break;
            default:
                break;
        }
    }

    /**
     * This loads the world data bundled as an asset file.
     *
     * @return This is the world data.
     * @throws IOException   This is thrown if there is an error reading from the asset file.
     * @throws JSONException This is thrown if there is an error parsing the JSON data from the asset file.
     */
    private WorldData loadWorldData() throws IOException, JSONException {
        if (BuildConfig.DEBUG) Log.d(TAG, "Loading world data from assets/world.json");
        return new WorldData(IOUtils.readAllJsonObject(this.getAssets().open(WORLD_DATA_FILENAME)));
    }

    /**
     * This loads previously saved data.
     *
     * @return This will be the previously saved data.
     * @throws IOException   This is thrown if there is an error reading the data from disk.
     * @throws JSONException This is thrown if there is an error parsing the JSON data.
     */
    private SaveData loadSavedData() throws IOException, JSONException {
        if (BuildConfig.DEBUG) Log.d(TAG, "Loading saved data");
        return new SaveData(IOUtils.readAllJsonObject(openFileInput(Static.CURRENT_GAME_FILE_NAME)));
    }

    /**
     * This is called when the player wishes to continue a previously saved game. This Activity transfers control of the
     * game to TownActivity.
     *
     * @see me.cybersensei.simplerpg.TownActivity
     */
    private void continuePreviousGame() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Continuing previous saved game");
        TownActivity.startTownActivity(this);
        finish();
    }

    /**
     * This creates a dialog to inform the player that the game file was corrupted and cannot be continued.
     */
    private void informLoadWorldDataFailure() {
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(R.string.er_loading_world_data)
                .setMessage(R.string.er_loading_world_data_explanation)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    /**
     * This creates a dialog to inform the player that the game file was corrupted and cannot be continued.
     */
    private void informGameFileCorrupted() {
        new AlertDialog.Builder(this)
                .setCancelable(true)
                .setTitle(R.string.er_loading_save_data)
                .setMessage(R.string.er_loading_save_data_explanation)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    /**
     * This is called when the player wishes to start a new game. This Activity transfers control of the game to
     * CreatePlayerActivity.
     *
     * @see me.cybersensei.simplerpg.CreatePlayerActivity
     */
    private void startNewGame() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Starting new game");
        CreatePlayerActivity.startCreatePlayerActivity(this);
        finish();
    }

    /**
     * This is the preferred was to start a StartGameActivity. It creates the Intent required and fills it with the
     * needed values.
     *
     * @param context This is the parent context.
     */
    public static void startStartGameActivity(Context context) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Request start StartGameActivity");
        context.startActivity(new Intent(context, StartGameActivity.class));
    }
}
