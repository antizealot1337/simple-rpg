package me.cybersensei.simplerpg.lib;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is a monster that can do battle.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.BattleBeing
 */
public class Creature extends BattleBeing {

    /**
     * This is the main constructor. This constructor doesn't do anything different from the parent class. All the
     * values collected here are passed unaltered to the super constructor.
     *
     * @param name         This is the name.
     * @param element      This is the element.
     * @param level        This is the level.
     * @param experience   This is the experience.
     * @param gold         This is the gold.
     * @param strength     This is the strength.
     * @param dexterity    This is the dexterity.
     * @param constitution This is the constitution.
     * @param agility      This is the agility.
     * @param damage       This is the damage.
     */
    public Creature(String name, String element, int level, int experience, int gold, int strength, int dexterity,
                    int constitution, int agility, int damage) {
        super(name, element, level, experience, gold, strength, dexterity, constitution, agility, damage);
    }

    /**
     * This constructor creates a Creature from a JSONObject. It is similar to creating a new object and calling
     * fromJSON but is safer.
     *
     * @param jsonObject This is the JSONObject that holds the Creature data.
     * @throws JSONException This is an exception thrown by fromJSON.
     */
    public Creature(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }
}
