package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.*;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Unit tests for Game.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.Game
 */
public class GameTest {
    // The name of the element
    private static final String ELEMENT_NAME = "Normal";

    // The name of the creature
    private static final String CREATURE_NAME = "Creature";

    // The name of the first town
    private static final String FIRST_TOWN_NAME = "First Town";

    // The name of the second town
    private static final String SECOND_TOWN_NAME = "Second Town";

    // The inn cost of the town
    private static final int INN_COST = 10;

    // This is the game for testing
    private Game game;

    @Before
    public void setUp() {
        // Create a list of elements
        ArrayList<ElementalType> elements = new ArrayList<>();
        elements.add(new ElementalType(ELEMENT_NAME));

        // Create a list of creatures
        ArrayList<Creature> creatures = new ArrayList<>();
        creatures.add(new Creature(CREATURE_NAME, ELEMENT_NAME, 1, 1, 1, 1, 1, 1, 1, 1));

        // Create a list of towns
        ArrayList<Town> towns = new ArrayList<>();
        Town firstTown = new Town(FIRST_TOWN_NAME, null, INN_COST);
        firstTown.addCreature(CREATURE_NAME);
        towns.add(firstTown);
        Town secondTown = new Town(SECOND_TOWN_NAME, null, INN_COST);
        towns.add(secondTown);

        this.game = new Game(new WorldData(elements, creatures, towns, FIRST_TOWN_NAME), null);
    }

    @Test
    public void testPlayer() {
        Assert.assertNull("Checking that player is currently null", this.game.getPlayer());

        Player player = new Player();
        this.game.setPlayer(player);
        Assert.assertNotNull("Checking that player is now set", this.game.getPlayer());
        Assert.assertEquals("Checking that the player is equal to the set player", player, this.game.getPlayer());
    }

    @Test
    public void testCurrentTown() {
        Assert.assertNotNull("Checking that the current town is not null", this.game.getCurrentTown());
        Assert.assertEquals("Checking the name of the current town", FIRST_TOWN_NAME, this.game.getCurrentTown().getName());
        this.game.setCurrentTown(this.game.findTownByName(SECOND_TOWN_NAME));
        Assert.assertNotNull("Checking that the current town is still not null", this.game.getCurrentTown());
        Assert.assertEquals("Checking the name of the current town", SECOND_TOWN_NAME, this.game.getCurrentTown().getName());
    }

    @Test
    public void testFindElementByName() {
        ElementalType element = this.game.findElementByName("");
        Assert.assertNull("Unknown element not found", element);

        element = this.game.findElementByName(ELEMENT_NAME);
        Assert.assertNotNull("Known element found", element);
        Assert.assertEquals("Checking found element name", ELEMENT_NAME, element.getName());
    }

    @Test
    public void testFindCreatureByName() {
        Creature creature = this.game.findCreatureByName("");
        Assert.assertNull("Unknown creature not found", creature);

        creature = this.game.findCreatureByName(CREATURE_NAME);
        Assert.assertNotNull("Known creature found", creature);
        Assert.assertEquals("Checking found creature name", CREATURE_NAME, creature.getName());
    }

    @Test
    public void testFindTownByName() {
        Town town = this.game.findTownByName("");
        Assert.assertNull("Unknown town not found", town);

        town = this.game.findTownByName(FIRST_TOWN_NAME);
        Assert.assertNotNull("Known town found", town);
        Assert.assertEquals("Checking found town name", FIRST_TOWN_NAME, town.getName());
    }

    @Test
    public void testCanStayAtInn() {
        Player player = new Player();
        this.game.setPlayer(player);
        player.setGold(0);
        Assert.assertFalse("Checking that the player  can't stay at the inn", this.game.canStayAtInn());
        player.setGold(INN_COST);
        Assert.assertTrue("Checking that the player can stay at the inn", this.game.canStayAtInn());
    }

    @Test
    public void testStayAtInn() {
        Player player = new Player();
        player.setGold(INN_COST);
        player.damage(player.getMaxHp());
        this.game.setPlayer(player);
        this.game.stayAtInn();
        Assert.assertEquals("Checking the player health after visit to inn", player.getMaxHp(), player.getHp());
        Assert.assertEquals("Checking the player gold after visit to inn", 0, player.getGold());
    }

    @Test
    public void testCheckForEncounter() {
        boolean encountered = false;

        for (int i = 0; i <= 20; i++) {
            String encounter = this.game.checkForEncounter();
            if (encounter != null) {
                encountered = true;
                Assert.assertNotNull("Checking that the creature name is valid", this.game.findCreatureByName(encounter));
            }
        }

        Assert.assertTrue("There should have been at least one encounter", encountered);

        encountered = false;
        this.game.setCurrentTown(this.game.findTownByName(SECOND_TOWN_NAME));

        for (int i = 0; i <= 20; i++) {
            String encounter = this.game.checkForEncounter();
            if (encounter != null) {
                encountered = true;
                Assert.assertNotNull("Checking that the creature name is valid", this.game.findCreatureByName(encounter));
            }
        }
        Assert.assertFalse("There should have been no encounters", encountered);
    }

    @Test
    public void testSave() throws JSONException, IOException {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
        this.game.setPlayer(new Player());
        this.game.save(outputStream);

        JSONObject jsonObject = new JSONObject(outputStream.toString());

        Assert.assertTrue("Checking there is player data", jsonObject.has(JSONSerializable.PLAYER));
        Assert.assertTrue("Checking there is current town data", jsonObject.has(JSONSerializable.CURRENT_TOWN));

        new SaveData(jsonObject);
    }
}
