package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.Creature;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for Creature.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.Creature
 */
public class CreatureTest {
    // The creature name
    private static final String NAME = "Creature";

    // The creature element
    private static final String ELEMENT = "Normal";

    // The creature level
    private static final int LEVEL = 1;

    // The creature experience
    private static final int EXPERIENCE = 50;

    // The creature gold
    private static final int GOLD = 2;

    // The creature strength
    private static final int STRENGTH = 3;

    // The creature dexterity
    private static final int DEXTERITY = 4;

    // The creature constitution
    private static final int CONSTITUTION = 5;

    // The creature agility
    private static final int AGILITY = 6;

    // The creature damage
    private static final int DAMAGE = 7;

    // This is the primary creature for testing with default values
    private Creature creature;

    @Before
    public void setUp() {
        this.creature = new Creature(NAME, ELEMENT, LEVEL, EXPERIENCE, GOLD, STRENGTH, DEXTERITY, CONSTITUTION, AGILITY,
                DAMAGE);
    }

    @Test
    public void testInitialValues() {
        Assert.assertEquals("Checking name", NAME, this.creature.getName());
        Assert.assertEquals("Checking element", ELEMENT, this.creature.getElement());
        Assert.assertEquals("Checking level", LEVEL, this.creature.getLevel());
        Assert.assertEquals("Checking experience", EXPERIENCE, this.creature.getExp());
        Assert.assertEquals("Checking gold", GOLD, this.creature.getGold());
        Assert.assertEquals("Checking strength", STRENGTH, this.creature.getStrength());
        Assert.assertEquals("Checking dexterity", DEXTERITY, this.creature.getDexterity());
        Assert.assertEquals("Checking constitution", CONSTITUTION, this.creature.getConstitution());
        Assert.assertEquals("Checking agility", AGILITY, this.creature.getAgility());
        Assert.assertEquals("Checking damage", DAMAGE, this.creature.getDamage());
    }
}
