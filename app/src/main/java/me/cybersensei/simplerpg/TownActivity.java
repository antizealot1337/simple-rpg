package me.cybersensei.simplerpg;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;
import me.cybersensei.simplerpg.fragments.TravelDialogFragment;
import me.cybersensei.simplerpg.lib.Town;

import java.io.FileOutputStream;
import java.util.Locale;

/**
 * This activity is the main activity for playing the game. It allows the player to examine their information, stay at
 * an inn if available, visit a field to fight monsters, or travel to other towns via roads.
 *
 * @author Christopher Pridgen
 */
public class TownActivity extends Activity implements View.OnClickListener, TravelDialogFragment.TravelListener {
    // The tag for logging
    private static final String TAG = TownActivity.class.getSimpleName();

    @Override
    protected void onCreate(Bundle bundle) {
        // Initialize the activity
        super.onCreate(bundle);
        setContentView(R.layout.activity_town);

        // Find children views
        Button playerInfo = findViewById(R.id.b_examine);
        Button inn = findViewById(R.id.b_inn);
        Button field = findViewById(R.id.b_field);
        Button travel = findViewById(R.id.b_travel);

        // Set listeners
        playerInfo.setOnClickListener(this);
        inn.setOnClickListener(this);
        field.setOnClickListener(this);
        travel.setOnClickListener(this);

        try {
            // Check for any nulls
            if (Static.game == null || Static.game.getCurrentTown() == null) {
                if (BuildConfig.DEBUG) Log.d(TAG, "Game not initialized properly");
                showTitleActivity();
            }
            travel(Static.game.getCurrentTown());
        } catch (NullPointerException nullPointerException) {
            showTitleActivity();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.town, menu);
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
        try {
            FileOutputStream fileOutputStream = openFileOutput(Static.CURRENT_GAME_FILE_NAME, 0);
            Static.game.save(fileOutputStream);
            fileOutputStream.close();
        } catch (Exception exception) {
            Log.e(TAG, "Error occurred while attempting to save state");
            exception.printStackTrace();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_examine:
                showExaminePlayer();
                break;
            case R.id.b_inn:
                // Check if the player can stay at the inn
                if (Static.game.canStayAtInn()) {
                    // Show the dialog to inform the player of the cost, their gold, and allow them to confirm to stay
                    // or not
                    showInnDialog();
                } else {
                    // Inform the player they don't have enough money to stay at the inn
                    showNoMoneyDialog();
                }
                break;
            case R.id.b_field:
                showFieldDialog();
                break;
            case R.id.b_travel:
                showTravelDialog(Static.game.getCurrentTown().getName());
                break;
            default:
                break;
        }
    }

    @Override
    public void travel(Town town) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Updating town to " + town.getName());
        setTitle(town.getName());
        Static.game.setCurrentTown(town);
    }

    /**
     * Start the StartGameActivity.
     *
     * @see me.cybersensei.simplerpg.StartGameActivity
     */
    private void showTitleActivity() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Returning to StartGameActivity to initialize game");
        StartGameActivity.startStartGameActivity(this);
        finish();
    }

    /**
     * The player requested to view the stats. Start the PlayerInfoActivity.
     *
     * @see me.cybersensei.simplerpg.PlayerInfoActivity
     */
    private void showExaminePlayer() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Examining the player");
        PlayerInfoActivity.startPlayerInfoActivity(this);
    }

    /**
     * The player requested to stay at an inn. Show the player a dialog to inform them of the inn price and the gold
     * they can spend.
     */
    private void showInnDialog() {
        Town currentTown = Static.game.getCurrentTown();
        if (BuildConfig.DEBUG) Log.d(TAG, "Showing the inn dialog");
        new AlertDialog.Builder(this)
                .setTitle(R.string.stay_at_inn)
                .setMessage(
                        String.format(getResources().getString(R.string.inn_format_string),
                                currentTown.getInnName(),
                                currentTown.getInnCost(),
                                Static.game.getPlayer().getGold()))
                .setCancelable(true)
                .setNegativeButton(R.string.no, null)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface param1DialogInterface, int param1Int) {
                        Static.game.stayAtInn();
                    }
                })
                .show();
    }

    /**
     * Inform the player they don't have enough gold to afford to stay at the current inn.
     */
    private void showNoMoneyDialog() {
        Town currentTown = Static.game.getCurrentTown();
        if (BuildConfig.DEBUG) Log.d(TAG, "Showing the no money dialog");
        new AlertDialog.Builder(this)
                .setTitle(R.string.oops_low_funds)
                .setCancelable(true)
                .setMessage(
                        String.format(getResources().getString(R.string.cannot_afford_inn_format_string),
                                currentTown.getInnName(),
                                currentTown.getInnCost(),
                                Static.game.getPlayer().getGold()))
                .setPositiveButton(R.string.shucks, null)
                .show();
    }

    /**
     * Check if the player encountered a creature. If so inform the player and proceed to the BattleActivity. If no
     * creature was encountered inform the player they encountered nothing.
     *
     * @see me.cybersensei.simplerpg.BattleActivity
     */
    private void showFieldDialog() {
        String name = Static.game.checkForEncounter();

        if (name == null) {
            nothingEncountered();
            return;
        }

        if (BuildConfig.DEBUG) Log.d(TAG, "Encountered " + name);

        Toast.makeText(this, String.format(Locale.getDefault(),
                getResources().getString(R.string.encountered_format_string), name), Toast.LENGTH_SHORT).show();

        BattleActivity.startBattleActivity(this, name);
    }

    /**
     * Inform the player that nothing was encountered.
     */
    private void nothingEncountered() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Showing the nothing encountered dialog");
        new AlertDialog.Builder(this)
                .setTitle(R.string.field_results)
                .setCancelable(true)
                .setMessage(R.string.no_encounter)
                .setPositiveButton(R.string.ok, null)
                .show();
    }

    /**
     * The player requested to travel. Show the player a dialog that includes all the available towns they may currently
     * travel to.
     *
     * @param townName This is the current town name
     */
    private void showTravelDialog(String townName) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Showing the travel dialog");
        getFragmentManager()
                .beginTransaction()
                .add(TravelDialogFragment.newTravelDialogFragment(townName),
                        TravelDialogFragment.class.getSimpleName())
                .commit();
    }

    /**
     * This is the preferred was to start a TownActivity. It creates the Intent required and fills it with the needed
     * values.
     *
     * @param context This is the parent context.
     */
    public static void startTownActivity(Context context) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Request start TownActivity");
        context.startActivity(new Intent(context, TownActivity.class));
    }
}
