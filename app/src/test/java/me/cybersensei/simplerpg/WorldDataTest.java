package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;

/**
 * Unit tests for WorldData
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.WorldData
 */
public class WorldDataTest {
    // The name of the element
    private static final String ELEMENT_NAME = "Normal";

    // The name of the creature
    private static final String CREATURE_NAME = "Creature";

    // The name of the town
    private static final String TOWN_NAME = "Town";

    // This is the world data for testing
    private WorldData worldData;

    @Before
    public void setUp() {
        // Create a list of elements
        ArrayList<ElementalType> elements = new ArrayList<>();
        elements.add(new ElementalType(ELEMENT_NAME));

        // Create a list of creatures
        ArrayList<Creature> creatures = new ArrayList<>();
        creatures.add(new Creature(CREATURE_NAME, ELEMENT_NAME, 1, 1, 1, 1, 1, 1, 1, 1));

        // Create a list of towns
        ArrayList<Town> towns = new ArrayList<>();
        towns.add(new Town(TOWN_NAME, "", 0));

        this.worldData = new WorldData(elements, creatures, towns, null);
    }

    @Test
    public void testFromJSON() throws JSONException {
        final String STARTING_TOWN = "Starting town";

        this.worldData = new WorldData(new ArrayList<ElementalType>(), new ArrayList<Creature>(), new ArrayList<Town>(),
                STARTING_TOWN);
        this.worldData.fromJSON(new JSONObject()
                .put(JSONSerializable.ELEMENTS, new JSONArray()
                        .put(new ElementalType(ELEMENT_NAME).toJSON())
                )
                .put(JSONSerializable.CREATURES, new JSONArray()
                        .put(new Creature(CREATURE_NAME, ELEMENT_NAME, 1, 1, 1, 1, 1, 1, 1, 1).toJSON())
                )
                .put(JSONSerializable.TOWNS, new JSONArray()
                        .put(new Town(TOWN_NAME, "", 0).toJSON())
                )
                .put(JSONSerializable.STARTING_TOWN, STARTING_TOWN)
        );

        Assert.assertEquals("Checking for the element", ELEMENT_NAME, this.worldData.findElement(ELEMENT_NAME).getName());
        Assert.assertEquals("Checking for the creature", CREATURE_NAME, this.worldData.findCreature(CREATURE_NAME).getName());
        Assert.assertEquals("Checking for the town", TOWN_NAME, this.worldData.findTown(TOWN_NAME).getName());
        Assert.assertEquals("Checking from the starting town name", STARTING_TOWN, this.worldData.getStartingTown());
    }

    @Test
    public void testToJSON() throws JSONException {
        JSONObject jsonObject = this.worldData.toJSON();

        Assert.assertTrue("Checking for elements field", jsonObject.has(JSONSerializable.ELEMENTS));
        Assert.assertEquals("Checking the number of elements", 1, jsonObject.getJSONArray(JSONSerializable.ELEMENTS).length());
        Assert.assertTrue("Checking for creatures field", jsonObject.has(JSONSerializable.CREATURES));
        Assert.assertEquals("Checking the number of creatures", 1, jsonObject.getJSONArray(JSONSerializable.CREATURES).length());
        Assert.assertTrue("Checking for towns field", jsonObject.has(JSONSerializable.TOWNS));
        Assert.assertEquals("Checking the number of towns", 1, jsonObject.getJSONArray(JSONSerializable.TOWNS).length());
        Assert.assertFalse("Checking for starting town field", jsonObject.has(JSONSerializable.STARTING_TOWN));

        jsonObject = new WorldData(new ArrayList<ElementalType>(), new ArrayList<Creature>(), new ArrayList<Town>(),
                TOWN_NAME).toJSON();

        Assert.assertTrue("Checking for starting town field", jsonObject.has(JSONSerializable.STARTING_TOWN));
        Assert.assertEquals("Checking the value of the starting town field", TOWN_NAME,
                jsonObject.getString(JSONSerializable.STARTING_TOWN));
    }

    @Test
    public void testGetStartingTown() {
        final String STARTING_TOWN = "Starting town";

        Assert.assertEquals("Checking the calculated starting town", TOWN_NAME, this.worldData.getStartingTown());

        this.worldData = new WorldData(new ArrayList<ElementalType>(), new ArrayList<Creature>(), new ArrayList<Town>(),
                STARTING_TOWN);

        Assert.assertEquals("Checking the explicitly set starting town", STARTING_TOWN, this.worldData.getStartingTown());
    }

    @Test
    public void testFindElement() {
        Assert.assertNull("Checking for null with bad element name", this.worldData.findElement(""));
        Assert.assertEquals("Checking that an element with the same name is returned", ELEMENT_NAME,
                this.worldData.findElement(ELEMENT_NAME).getName());
    }

    @Test
    public void testFindCreature() {
        Assert.assertNull("Checking for null with bad creature name", this.worldData.findCreature(""));
        Assert.assertEquals("Checking that a creature with the same name is returned", CREATURE_NAME,
                this.worldData.findCreature(CREATURE_NAME).getName());
    }

    @Test
    public void testFindTown() {
        Assert.assertNull("Checking for null with bad town name", this.worldData.findTown(""));
        Assert.assertEquals("Checking that a creature with the same name is returned", TOWN_NAME,
                this.worldData.findTown(TOWN_NAME).getName());
    }
}
