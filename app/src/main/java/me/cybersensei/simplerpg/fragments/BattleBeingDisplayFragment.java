package me.cybersensei.simplerpg.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import me.cybersensei.simplerpg.BuildConfig;
import me.cybersensei.simplerpg.R;

import java.util.Locale;

/**
 * This fragment is responsible for display information about a BattleBeing during a battle.
 *
 * @author Christopher Pridgen
 */
public class BattleBeingDisplayFragment extends Fragment {
    // The tag for logging
    private static final String TAG = BattleBeingDisplayFragment.class.getSimpleName();

    // This is the name TextView
    private TextView name;

    // This is the health ProgressBar
    private ProgressBar healthBar;

    // The is the health TextView
    private TextView health;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the view
        View view = inflater.inflate(R.layout.fragment_battle_being_display, container, false);

        // Find the children views
        name = view.findViewById(R.id.tv_being_name);
        healthBar = view.findViewById(R.id.pb_being_health);
        health = view.findViewById(R.id.tv_being_health);

        return view;
    }

    /**
     * This will set the provided value to the name TextView.
     *
     * @param name This is the new value of name.
     */
    public void setName(String name) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Updating name to " + name);
        this.name.setText(name);
    }

    /**
     * This will update the health text and bar with the provided values.
     *
     * @param current This is the current health value.
     * @param max     This is the max health value.
     */
    public void updateHealth(int current, int max) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Update the health to " + current + "/" + max);
        updateHealthBar(current, max);
        updateHealthText(current, max);
    }

    /**
     * This will update the health bar with the provided values.
     *
     * @param current This is the current progress.
     * @param max     This is the max value.
     */
    private void updateHealthBar(int current, int max) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Updating health bar");
        this.healthBar.setProgress(current);
        this.healthBar.setMax(max);
    }

    /**
     * This will update the health text with a formatted string %d/%d where the first value is the current and the
     * second is the max.
     *
     * @param current This is the current value.
     * @param max     This is the max value.
     */
    private void updateHealthText(int current, int max) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Update the health text");
        this.health.setText(String.format(Locale.getDefault(), getResources().getString(R.string.health_format_string),
                current, max));
    }
}
