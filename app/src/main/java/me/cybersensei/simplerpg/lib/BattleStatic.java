package me.cybersensei.simplerpg.lib;

import java.util.Locale;

/**
 * This is just a utility class with some useful and testable battle utility functions.
 *
 * @author Christopher Pridgen
 */
public class BattleStatic {
    // This is a static BattleResult used to reduce garbage
    private static final BattleResult result = new BattleResult();

    /**
     * Calculate the elemental multiplier based on element strengths and weaknesses.
     *
     * @param defenderElement This is the defender's elemental type.
     * @param attackerElement This is the attacker's elemental type.
     * @return This is the resulting elemental damage multiplier value.
     */
    public static double calculateElementMultiplier(ElementalType defenderElement, ElementalType attackerElement) {
        return attackerElement.calculateDamageMultiplier(defenderElement);
    }

    /**
     * This function handles all aspects of one BattleBeing attacking another. It calculates attack success, it applies
     * damage, and returns the result.
     *
     * @param attacker         This is the attacker for attack and damage calculations.
     * @param defender         This is the defender for defense calculations and the application of damage.
     * @param damageMultiplier This is a damage multiplier used to increase or decrease the damage.
     * @return This is the result of the attack.
     */
    public static BattleResult attack(BattleBeing attacker, BattleBeing defender, double damageMultiplier) {
        // Reset the result
        result.reset();

        // Check for a successful attack
        if (Game.random.nextInt(attacker.getLevel() + attacker.getAttack()) > Game.random.nextInt(defender.getLevel() + defender.getDamage())) {
            // Calculate the damage
            int damage = (int) Math.ceil(attacker.getDamage() * damageMultiplier);

            // Apply the damage to the defender
            defender.damage(damage);

            // Update the result to be a successful attack
            result.attack(attacker.getName(), defender.getName(), true,
                    String.format(Locale.getDefault(),
                            "%s attacked %s for %d point(s) of damage", attacker.getName(),
                            defender.getName(), damage));

            // Return the result
            return result;
        }

        // Update the result to be a failed attack
        result.attack(attacker.getName(), defender.getName(), false,
                String.format(Locale.getDefault(), "%s failed to hit %s",
                        attacker.getName(), defender.getName()));

        // Return the result
        return result;
    }

    /**
     * This function handles all aspects of one BattleBeing attempting to flee from another. It calculates the speeds
     * and determines if the coward escapes or not.
     *
     * @param coward   This is the coward used for speed calculations .
     * @param defender This is the pursuer used for speed calculations.
     * @return This is the result of the flee.
     */
    public static BattleResult flee(BattleBeing coward, BattleBeing defender) {
        result.reset();
        if (coward.getSpeed() + Game.random.nextInt(coward.getSpeed() + 1) > defender.getSpeed() + Game.random.nextInt(defender.getSpeed() + 1)) {
            result.flee(coward.getName(), true,
                    String.format(Locale.getDefault(), "%s fled.", coward.getName()));
            return result;
        }
        result.flee(coward.getName(), false,
                String.format(Locale.getDefault(), "%s failed to flee.",
                        coward.getName()));
        return result;
    }

    /**
     * The functions handles checking for defeat, It will generate a BattleResult no matter if the BattleBeing is
     * defeated or not. If the BattleBeing is vanquished (their health dropped below zero) then the result will be
     * successful. If they are still alive then the result will not be successful.
     *
     * @param being This is the being to check to see if they still live.
     * @return This is the result of the check for defeat. If the result is successful then the being is dead. If it is
     * not they they yet live.
     */
    public static BattleResult defeated(BattleBeing being) {
        result.reset();
        String name = being.getName();
        if (being.getHp() <= 0) {
            result.defeat(name, true, String.format(Locale.getDefault(),
                    "%s defeated", name));
            return result;
        }
        result.defeat(name, false, String.format(Locale.getDefault(),
                "%s defeated", name));
        return result;
    }
}
