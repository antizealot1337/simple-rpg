package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.JSONSerializable;
import me.cybersensei.simplerpg.lib.Town;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for TownTest.
 *
 * @author Christopher Prigen
 * @see me.cybersensei.simplerpg.lib.Town
 */
public class TownTest {
    // The town name
    private static final String NAME = "Unnamed";

    // The town inn name
    private static final String INN_NAME = "Inn";

    // The town inn cost
    private static final int INN_COST = 1;

    // This is the primary town for testing with default values
    private Town town;

    @Before
    public void setUp() {
        this.town = new Town(NAME, INN_NAME, INN_COST);
    }

    @Test
    public void testInitialValues() {
        Assert.assertEquals("Checking town name", NAME, this.town.getName());
        Assert.assertEquals("Checking inn name", INN_NAME, this.town.getInnName());
        Assert.assertEquals("Checking inn cost", INN_COST, this.town.getInnCost());
        Assert.assertNotNull("Checking creatureInField is not null", this.town.getEncounters());
        Assert.assertNotNull("Checking roads is not null", this.town.getRoads());
    }

    @Test
    public void testFromJSON() throws JSONException {
        final String NAME = "Test";
        final String INN_NAME = "Test Inn";
        final int INN_COST = 2;
        final String CREATURE = "Test Creature";
        final String ROAD_NAME = "Test Road";
        final boolean ROAD_ENABLED = true;

        this.town.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(JSONSerializable.INN_NAME, INN_NAME)
                .put(JSONSerializable.INN_COST, INN_COST)
                .put(JSONSerializable.FIELD_CREATURES, new JSONArray().put(CREATURE))
                .put(JSONSerializable.ROADS, new JSONArray()
                        .put(new JSONObject()
                                .put(JSONSerializable.END_TOWN, ROAD_NAME)
                                .put(JSONSerializable.ENABLED, ROAD_ENABLED)
                        )
                )
        );

        Assert.assertEquals("Checking town name", NAME, this.town.getName());
        Assert.assertEquals("Checking town inn name", INN_NAME, this.town.getInnName());
        Assert.assertEquals("Checking town inn cost", INN_COST, this.town.getInnCost());
        Assert.assertEquals("Checking number of field creatures", 1, this.town.getEncounters().size());
        Assert.assertEquals("Checking first creature", CREATURE, this.town.getEncounters().get(0));
        Assert.assertEquals("Checking number of roads", 1, this.town.getRoads().size());
        Assert.assertEquals("Checking first road destination", ROAD_NAME, this.town.getRoads().get(0).getName());
        Assert.assertEquals("Checking first road enabled", ROAD_ENABLED, this.town.getRoads().get(0).getEnabled());
    }

    @Test
    public void testToJSON() throws JSONException {
        final String NAME = "TOWN";
        final String INN_NAME = "Town Inn";
        final int INN_COST = 5;
        final String CREATURE_NAME = "Bob";
        final String ROAD_NAME = "Other Town";
        final boolean ROAD_ENABLED = true;

        this.town.setName(NAME);
        this.town.setInnName(INN_NAME);
        this.town.setInnCost(INN_COST);
        this.town.addCreature(CREATURE_NAME);
        this.town.addRoad(ROAD_NAME, ROAD_ENABLED);
        JSONObject jSONObject = this.town.toJSON();
        Assert.assertEquals("Checking name in json", NAME, jSONObject.getString(JSONSerializable.NAME));
        Assert.assertEquals("Checking inn name in json", INN_NAME, jSONObject.getString(JSONSerializable.INN_NAME));
        Assert.assertEquals("Checking inn cost in json", INN_COST, jSONObject.getInt(JSONSerializable.INN_COST));
        Assert.assertEquals("Checking number of encounters in json", 1, jSONObject
                .getJSONArray(JSONSerializable.FIELD_CREATURES)
                .length()
        );
        Assert.assertEquals("Checking first encounter in json", CREATURE_NAME, jSONObject
                .getJSONArray(JSONSerializable.FIELD_CREATURES)
                .get(0)
        );
        Assert.assertEquals("Checking number of roads in json", 1, jSONObject
                .getJSONArray(JSONSerializable.ROADS)
                .length()
        );
        Assert.assertEquals("Checking first road name in json", ROAD_NAME, jSONObject
                .getJSONArray(JSONSerializable.ROADS)
                .getJSONObject(0)
                .getString(JSONSerializable.END_TOWN)
        );
        Assert.assertEquals("Checking first road enabled json", ROAD_ENABLED, jSONObject
                .getJSONArray(JSONSerializable.ROADS)
                .getJSONObject(0)
                .getBoolean(JSONSerializable.ENABLED)
        );
    }

    @Test
    public void testInnName() {
        final String INN_NAME = "InnName";
        this.town.setInnName(INN_NAME);
        Assert.assertEquals("Checking inn name", INN_NAME, this.town.getInnName());
    }

    @Test
    public void testInnCost() {
        final int INN_COST = 3;
        this.town.setInnCost(INN_COST);
        Assert.assertEquals("Checking inn cost", INN_COST, this.town.getInnCost());
    }

    @Test
    public void testAddCreature() {
        final String CREATURE_NAME = "Bob";
        this.town.addCreature(CREATURE_NAME);
        Assert.assertEquals("Checking encounters size", 1, this.town.getEncounters().size());
        Assert.assertTrue("Checking encounter contains " + CREATURE_NAME, this.town.getEncounters().contains(CREATURE_NAME));
    }

    @Test
    public void testAddRoad() {
        final String ROAD_NAME = "Other Town";
        final boolean ROAD_ENABLED = true;
        this.town.addRoad(ROAD_NAME, ROAD_ENABLED);
        Assert.assertEquals("Checking roads size", 1, this.town.getRoads().size());
        Assert.assertEquals("Checking road name", ROAD_NAME, this.town.getRoads().get(0).getName());
        Assert.assertEquals("Checking road enabled", ROAD_ENABLED, this.town.getRoads().get(0).getEnabled());
    }

    @Test
    public void testHasEncounters() {
        Assert.assertFalse("Checking for encounters", this.town.hasEncounters());
        this.town.addCreature("Creature");
        Assert.assertTrue("Checking for encounters", this.town.hasEncounters());
    }
}
