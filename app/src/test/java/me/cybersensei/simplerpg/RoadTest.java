package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.JSONSerializable;
import me.cybersensei.simplerpg.lib.Road;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for Road.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.Road
 */
public class RoadTest {
    // The road name
    private static final String NAME = "Road";

    // The road enabled value
    private static final boolean ENABLED = true;

    // This is the primary road for testing with default values
    private Road road;

    @Before
    public void setUp() {
        this.road = new Road(NAME, ENABLED);
    }

    @Test
    public void testInitialVales() {
        Assert.assertEquals("Checking end town", NAME, this.road.getName());
        Assert.assertEquals("Checking enabled", ENABLED, this.road.getEnabled());
    }

    @Test
    public void testFromJSON() throws JSONException {
        final String DESTINATION = "Bob";
        final boolean ENABLED = true;

        this.road.fromJSON(new JSONObject()
                .put(JSONSerializable.END_TOWN, DESTINATION)
                .put(JSONSerializable.ENABLED, ENABLED)
        );
        Assert.assertEquals("Checking end town", DESTINATION, this.road.getName());
        Assert.assertEquals("Checking enabled", ENABLED, this.road.getEnabled());
    }

    @Test
    public void testToJSON() throws JSONException {
        this.road = new Road("Bob", true);
        JSONObject jSONObject = this.road.toJSON();
        Assert.assertEquals("Checking end town", "Bob", jSONObject.getString(JSONSerializable.END_TOWN));
        Assert.assertTrue("Checking enabled", jSONObject.getBoolean(JSONSerializable.ENABLED));
    }

    @Test
    public void testName() {
        this.road.setName("Test");
        Assert.assertEquals("Checking name", "Test", this.road.getName());
    }

    @Test
    public void testEnabled() {
        this.road.setEnabled(true);
        Assert.assertTrue("Checking enabled", this.road.getEnabled());
    }
}
