# Simple RPG

Simple RPG is a very simplistic text and button based role-playing Android app/game. You can create a character, fight
monsters, visit an inn, travel to other towns, and level up. What more could you ask for? B)

## What you see here

This is the legacy-fixes branch. This contains fixes, and a few more quality improvements over the legacy branch. This
branch stops short of being a major update though.

## History

This game was made in the early 2010s. Given the time period it was probably made with Eclipse + ADT. It was published
to the Google Play Store as a closed beta and received updates and features. Eventually, though development of the game
stalled as it was more of a prototype than full game. Later the 2011 MacBook Pro that housed the original source code
crashed.  Since opening the machine is annoying, and the APFS filesystem is rather new and would need to be accessed by
a Linux machine, the source code was considered for all intents and purposes lost.

Recently the last published version of the app was installed, and the APK was extracted. It was the disassembled and
decompiled. After extracting the source and resources this project was created. It uses the gradle build system rather
than Apache ANT from the Eclipse + ADT era. Then the process of preparing for upload began. The source in some cases was
obscure and in others missing. This was probably due to compiler optimizations and the disassembler. After this was
done, documentation was added to the code. There were a few fixes applied, but those were mostly to get the apk to
compile. The design decisions of the original source code were left intact and unaltered.

## License

Copyright 2021 Christopher Pridgen
Licensed under the terms of the MIT License. See LICENSE file for license terms.
