package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.JSONSerializable;
import me.cybersensei.simplerpg.lib.Player;
import me.cybersensei.simplerpg.lib.SaveData;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for SaveData.
 *
 * @author Christopher Pridge
 * @see me.cybersensei.simplerpg.lib.SaveData
 */
public class SaveDataTest {
    // The name of the player
    private static final String PLAYER_NAME = "Player";

    // The name of the current town
    private static final String CURRENT_TOWN = "Current town";

    // The save data for testing
    private SaveData saveData;

    @Before
    public void setUp() {
        this.saveData = new SaveData(new Player(PLAYER_NAME, "", 1, 1, 1, 1, 1, 1, 1, 1, 0), CURRENT_TOWN);
    }

    @Test
    public void testFromJSON() throws JSONException {
        final String PLAYER_NAME = "Player";
        final String ELEMENT = "";
        final int LEVEL = 1;
        final int EXPERIENCE = 1;
        final int GOLD = 1;
        final int STRENGTH = 1;
        final int DEXTERITY = 1;
        final int CONSTITUTION = 1;
        final int AGILITY = 1;
        final int DAMAGE = 1;
        final int STAT_POINTS = 0;
        final String CURRENT_TOWN = "Current town";

        this.saveData.fromJSON(new JSONObject()
                .put(JSONSerializable.PLAYER, new JSONObject()
                        .put(JSONSerializable.NAME, PLAYER_NAME)
                        .put(JSONSerializable.ELEMENTAL_TYPE, ELEMENT)
                        .put(JSONSerializable.HP, 0)
                        .put(JSONSerializable.LEVEL, LEVEL)
                        .put(JSONSerializable.EXP, EXPERIENCE)
                        .put(JSONSerializable.GOLD, GOLD)
                        .put(JSONSerializable.STRENGTH, STRENGTH)
                        .put(JSONSerializable.DEXTERITY, DEXTERITY)
                        .put(JSONSerializable.CONSTITUTION, CONSTITUTION)
                        .put(JSONSerializable.AGILITY, AGILITY)
                        .put(JSONSerializable.DAMAGE, DAMAGE)
                        .put(JSONSerializable.STAT_POINTS, STAT_POINTS))
                .put(JSONSerializable.CURRENT_TOWN, CURRENT_TOWN)
        );

        Assert.assertEquals("Checking the player data", PLAYER_NAME, this.saveData.getPlayer().getName());
        Assert.assertEquals("Checking the current town", CURRENT_TOWN, this.saveData.getCurrentTown());
    }

    @Test
    public void testFromJsonOldCurrentTownField() throws JSONException {
        final String PLAYER_NAME = "Player";
        final String ELEMENT = "";
        final int LEVEL = 1;
        final int EXPERIENCE = 1;
        final int GOLD = 1;
        final int STRENGTH = 1;
        final int DEXTERITY = 1;
        final int CONSTITUTION = 1;
        final int AGILITY = 1;
        final int DAMAGE = 1;
        final int STAT_POINTS = 0;
        final String CURRENT_TOWN = "Current town";

        this.saveData.fromJSON(new JSONObject()
                .put(JSONSerializable.PLAYER, new JSONObject()
                        .put(JSONSerializable.NAME, PLAYER_NAME)
                        .put(JSONSerializable.ELEMENTAL_TYPE, ELEMENT)
                        .put(JSONSerializable.HP, 0)
                        .put(JSONSerializable.LEVEL, LEVEL)
                        .put(JSONSerializable.EXP, EXPERIENCE)
                        .put(JSONSerializable.GOLD, GOLD)
                        .put(JSONSerializable.STRENGTH, STRENGTH)
                        .put(JSONSerializable.DEXTERITY, DEXTERITY)
                        .put(JSONSerializable.CONSTITUTION, CONSTITUTION)
                        .put(JSONSerializable.AGILITY, AGILITY)
                        .put(JSONSerializable.DAMAGE, DAMAGE)
                        .put(JSONSerializable.STAT_POINTS, STAT_POINTS))
                .put(SaveData.CURRENT_TOWN, CURRENT_TOWN)
        );

        Assert.assertEquals("Checking the player data", PLAYER_NAME, this.saveData.getPlayer().getName());
        Assert.assertEquals("Checking the current town", CURRENT_TOWN, this.saveData.getCurrentTown());
    }

    @Test
    public void testToJSON() throws JSONException {
        JSONObject jsonObject = this.saveData.toJSON();

        Assert.assertTrue("Checking that player data is available", jsonObject.has(JSONSerializable.PLAYER));
        Assert.assertEquals("Checking the player name", PLAYER_NAME, jsonObject.getJSONObject(JSONSerializable.PLAYER).getString(JSONSerializable.NAME));
        Assert.assertTrue("Checking that the current town is available", jsonObject.has(JSONSerializable.CURRENT_TOWN));
        Assert.assertEquals("Checking the current town", CURRENT_TOWN, jsonObject.getString(JSONSerializable.CURRENT_TOWN));
    }

    @Test
    public void testPlayer() {
        Assert.assertEquals("Checking the player data", PLAYER_NAME, this.saveData.getPlayer().getName());

        final String PLAYER_NAME = "Different player name";
        this.saveData.setPlayer(new Player(PLAYER_NAME, "", 1, 1, 1, 1, 1, 1, 1, 1, 1));
        Assert.assertEquals("Checking the new player data", PLAYER_NAME, this.saveData.getPlayer().getName());
    }

    @Test
    public void testCurrentTown() {
        Assert.assertEquals("Checking the current town", CURRENT_TOWN, this.saveData.getCurrentTown());

        final String CURRENT_TOWN = "Different town";
        this.saveData.setCurrentTown(CURRENT_TOWN);
        Assert.assertEquals("Checking the new current town", CURRENT_TOWN, this.saveData.getCurrentTown());
    }
}
