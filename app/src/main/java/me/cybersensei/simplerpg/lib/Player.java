package me.cybersensei.simplerpg.lib;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the player data. It adds some functionality to humanoid with regards to gold, leveling, and stat points.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.BattleBeing
 */
public class Player extends BattleBeing {
    /**
     * This is the field name for stat points.
     */
    public static final String STAT_POINTS = "statPoints";

    /**
     * This is the legacy value for STAT_POINTS.
     */
    public static final String STAT_POINTS_LEGACY = "stat points";

    // These are stat points that have yet to be spent
    private int statPoints;

    /**
     * This is the default constructor. It sets the name to "NewCharacter". It sets the element to "Normal". It sets the
     * level and damage to one. It sets the experience to fifty. It sets the gold to eight. It sets the strength,
     * dexterity, constitution, and agility to three. It sets the statPoints to nine.
     */
    public Player() {
        this("NewCharacter", "Normal", 1, 50, 8, 3, 3, 3, 3, 1, 9);
    }

    /**
     * This is the primary constructor. Most of the values are passed to the parent class except the stat points that
     * are specific to the Player.
     *
     * @param name         This is the name.
     * @param element      This is the element.
     * @param level        This is the level.
     * @param experience   This is the experience.
     * @param gold         This is the gold.
     * @param strength     This is the strength.
     * @param dexterity    This is the dexterity.
     * @param constitution This is the constitution.
     * @param agility      This is the agility.
     * @param damage       This is the damage.
     * @param statPoints   This is the amount of stat points.
     */
    public Player(String name, String element, int level, int experience, int gold, int strength, int dexterity, int constitution, int agility, int damage, int statPoints) {
        super(name, element, level, experience, gold, strength, dexterity, constitution, agility, damage);
        this.statPoints = statPoints;
    }

    /**
     * This constructor creates a Player from a JSONObject. It is similar to creating a new object and calling fromJSON
     * but is safer.
     *
     * @param jsonObject This is the JSONObject that holds the Player data.
     * @throws JSONException This is an exception thrown by fromJSON.
     */
    public Player(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }

    @Override
    public String toString() {
        return super.toString() + " stat points: " + this.statPoints;
    }

    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);
        if (jsonObject.has(STAT_POINTS_LEGACY)) {
            this.statPoints = jsonObject.getInt(STAT_POINTS_LEGACY);
        } else {
            this.statPoints = jsonObject.getInt(STAT_POINTS);
        }
        if (this.element == null)
            this.element = "Normal";
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        return super.toJSON().put(STAT_POINTS, this.statPoints);
    }

    /**
     * Get the stat points.
     *
     * @return This is the number of stat points.
     */
    public int getStatPoints() {
        return this.statPoints;
    }

    /**
     * Set the stat points.
     *
     * @param statPoints This is the new value for stat points.
     */
    public void setStatPoints(int statPoints) {
        this.statPoints = statPoints;
    }

    /**
     * Adds experience to the character. This actually reduces the experience the player has. When the player's
     * experience falls below zero then the character levels up and their experience is recalculated. This functions can
     * handle levelling up multiple times at once.
     *
     * @param experience The amount of experience to add.
     * @return This is the number of levels gained.
     */
    public int addExp(int experience) {
        this.exp -= experience;
        int levels = 0;
        while (this.exp < 0) {
            levels++;
            this.exp += this.level * 100;
            this.level++;
            this.hp++;
            this.strength++;
            this.dexterity++;
            this.agility++;
            this.constitution++;
            this.statPoints += 3;
        }
        return levels;
    }

    /**
     * Checks to see if the player can afford something.
     *
     * @param cost This is the cost of something.
     * @return This will be true if the player can afford the cost or false if not.
     */
    public boolean hasEnoughGold(int cost) {
        return (this.gold - cost >= 0);
    }

    /**
     * Add and amount to the player's gold. If the amount provided is less than zero, then the gold amount is unaltered.
     *
     * @param amount This is the amount of gold to add.
     */
    public void addGold(int amount) {
        if (amount < 0) {
            return;
        }
        this.gold += amount;
    }

    /**
     * Charge the player gold by deducting it from their current amount. If the player does not have enough then
     * nothing is deducted.
     *
     * @param amount The amount of gold to deduct from the player's gold.
     * @return This returns true if the amount was deducted and false if the player didn't have enough and the amount
     * was not deducted.
     */
    public boolean chargeGold(int amount) {
        if (amount < 0 || !hasEnoughGold(amount))
            return false;
        this.gold -= amount;
        return true;
    }
}
