package me.cybersensei.simplerpg;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import me.cybersensei.simplerpg.fragments.PlayerStatDistributionFragment;
import me.cybersensei.simplerpg.lib.Player;

import java.util.Locale;

/**
 * This activity shows the player information. If there are stat points available to be spent then the player may spend
 * them while here.
 */
public class PlayerInfoActivity extends Activity
        implements View.OnClickListener, PlayerStatDistributionFragment.StatChangedListener {
    // The tag for logging
    private static final String TAG = PlayerInfoActivity.class.getSimpleName();

    // The health TextView
    private TextView health;

    // The health ProgressBar
    private ProgressBar healthBar;

    // The stat distribution fragment
    private PlayerStatDistributionFragment playerStatFragment;

    // The cached instance of player
    private final Player player = Static.game.getPlayer();

    @Override
    protected void onCreate(Bundle bundle) {
        // Initialize the activity
        super.onCreate(bundle);
        setContentView(R.layout.activity_player_info);

        // Find and update the name TextView
        TextView name = findViewById(R.id.tv_name_value);
        name.setText(this.player.getName());

        // Find and update the level TextView
        TextView level = findViewById(R.id.tv_level_value);
        updateTextView(level, this.player.getLevel());

        // Find and update the exp TextView
        TextView exp = findViewById(R.id.tv_exp_value);
        updateTextView(exp, this.player.getExp());

        // Find and update the gold TextView
        TextView gold = findViewById(R.id.tv_gold_value);
        updateTextView(gold, this.player.getGold());

        // Find and update the health TextView
        this.health = findViewById(R.id.tv_health_value);
        updateHealthTextView();

        // Find and update the health ProgressBar
        this.healthBar = findViewById(R.id.pb_player_health);
        updateHealthBar();

        // Find the stat distribution fragment and update it with the player's stats
        this.playerStatFragment = (PlayerStatDistributionFragment) getFragmentManager().findFragmentById(R.id.f_stats);
        this.playerStatFragment.setStatsFromPlayer(this.player);

        findViewById(R.id.b_done).setOnClickListener(this);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.player_info, menu);
        return true;
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_done:
                // Update the stats in case the player has spent some stat points
                this.playerStatFragment.applyStatsToPlayer(Static.game.getPlayer());
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void statsUpdated() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Stats updated");
        updateHealthBar();
        updateHealthTextView();
    }

    /**
     * Update the health progress bar view.
     */
    private void updateHealthBar() {
        this.healthBar.setMax(this.player.getMaxHp());
        this.healthBar.setProgress(this.player.getHp());
    }

    /**
     * Update the health text view.
     */
    private void updateHealthTextView() {
        this.health.setText(String.format(Locale.getDefault(),
                getResources().getString(R.string.health_format_string),
                this.player.getHp(), this.player.getMaxHp()));
    }

    /**
     * Update a text view with the integer value. This uses String.format with the default locale.
     *
     * @param textView This is the text view to update.
     * @param value    This is the value to be converted to a String and provided to the TextView.
     * @see java.util.Locale
     */
    private void updateTextView(TextView textView, int value) {
        textView.setText(String.format(Locale.getDefault(), "%d", value));
    }

    /**
     * This is the preferred was to start a PlayerInfoActivity. It creates the Intent required and fills it with the
     * needed values.
     *
     * @param context This is the parent context.
     */
    public static void startPlayerInfoActivity(Context context) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Request start PlayerInfoActivity");

        context.startActivity(new Intent(context, PlayerInfoActivity.class));
    }
}
