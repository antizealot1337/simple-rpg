package me.cybersensei.simplerpg.fragments;

import android.app.Activity;
import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import me.cybersensei.simplerpg.BuildConfig;
import me.cybersensei.simplerpg.R;
import me.cybersensei.simplerpg.Static;
import me.cybersensei.simplerpg.lib.Road;
import me.cybersensei.simplerpg.lib.Town;

/**
 * This dialog fragment allows the player to travel from one town to another via the road. Any Activity that creates
 * this dialog fragment must implements the TravelListener interface.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.Town
 * @see me.cybersensei.simplerpg.lib.Road
 * @see me.cybersensei.simplerpg.fragments.TravelDialogFragment.TravelListener
 */
public class TravelDialogFragment extends DialogFragment implements AdapterView.OnItemClickListener {
    // The tag for logging
    private static final String TAG = TravelDialogFragment.class.getSimpleName();

    // This is the tag to pass the current down name to the fragment
    public static final String TOWN_NAME = "town name";

    // The listener for this fragment
    private TravelListener listener;

    // The current town
    private Town town;

    @Override
    public void setArguments(Bundle bundle) {
        super.setArguments(bundle);
        // Get the current town from the bundle
        this.town = Static.game.findTownByName(bundle.getString(TOWN_NAME));
    }

    @Override
    public void onAttach(Activity activity) {
        // Cast the attached activity to the listener
        this.listener = (TravelListener) activity;
        super.onAttach(activity);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        // Set the dialog title
        getDialog().setTitle(R.string.travel);

        //Inflate the layout
        View view = layoutInflater.inflate(R.layout.fragment_travel_dialog, viewGroup, false);

        // Create an array adapter
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(getActivity(), android.R.layout.simple_list_item_1);

        // Fill the array adapter from the roads
        for (Road road : this.town.getRoads()) {
            if (road.getEnabled()) {
                arrayAdapter.add(road.getName());
            }
        }

        // Find the list view child
        ListView listView = view.findViewById(R.id.lv_travel);

        // Set the click listener
        listView.setOnItemClickListener(this);

        // Set the array adapter
        listView.setAdapter(arrayAdapter);

        return view;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Find the town
        this.town = Static.game.findTownByName((this.town.getRoads().get(position)).getName());

        if (BuildConfig.DEBUG) Log.d(TAG, "Player selected " + town.getName() + " to travel to");

        // Inform the listener
        this.listener.travel(this.town);

        // Dismiss the dialog
        dismiss();
    }

    /**
     * This will create a TravelDialogFragment and provide it with the required values.
     *
     * @param townName This is the name of the current town.
     * @return This will be a fragment ready to be added to the view tree.
     */
    public static TravelDialogFragment newTravelDialogFragment(String townName) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Creating new TravelDialogFragment");

        // Create the args
        Bundle args = new Bundle();
        args.putString(TOWN_NAME, townName);

        // Create the fragment
        TravelDialogFragment fragment = new TravelDialogFragment();
        fragment.setArguments(args);

        return fragment;
    }

    /**
     * This interface allows the dialog to communicate the intent to travel.
     */
    public interface TravelListener {
        /**
         * Called when the player has selected an area to travel to.
         *
         * @param town This is the area the player wishes to visit.
         * @see me.cybersensei.simplerpg.lib.Town
         */
        void travel(Town town);
    }
}
