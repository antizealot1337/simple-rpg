package me.cybersensei.simplerpg.lib;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the dynamic persistent data for the game.
 *
 * @author Christopher Pridgen
 */
public class SaveData implements JSONSerializable {
    /**
     * This is the field name for player.
     */
    public static final String PLAYER = "player";

    /**
     * This is the field name for the current town.
     */
    public static final String CURRENT_TOWN = "currentTown";

    /**
     * This is the legacy value of CURRENT_TOWN.
     */
    public static final String CURRENT_TOWN_LEGACY = "current town";

    // This is the player data
    private Player player;

    // This is the name of the current town
    private String currentTown;

    /**
     * This is the primary constructor for save data. It accepts the player data and the current town name.
     *
     * @param player      This is the player data.
     * @param currentTown This is the name of the current town.
     */
    public SaveData(Player player, String currentTown) {
        this.player = player;
        this.currentTown = currentTown;
    }

    /**
     * This constructor creates SaveData from a JSONObject. It is similar to creating a new object and calling fromJSON
     * but is safer.
     *
     * @param jsonObject This is the JSONObject that holds the Town data.
     * @throws JSONException This is an exception thrown by fromJSON.
     */
    public SaveData(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }

    @Override
    public String toString() {
        return "SaveData{" +
                "player=" + player +
                ", currentTown='" + currentTown + '\'' +
                '}';
    }

    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        this.player = new Player(jsonObject.getJSONObject(PLAYER));
        if (jsonObject.has(CURRENT_TOWN_LEGACY)) {
            this.currentTown = jsonObject.getString(CURRENT_TOWN_LEGACY);
        } else {
            this.currentTown = jsonObject.getString(CURRENT_TOWN);
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        return new JSONObject()
                .put(PLAYER, this.player.toJSON())
                .put(CURRENT_TOWN, this.currentTown);
    }

    /**
     * This will return the player data.
     *
     * @return This is the player data.
     */
    public Player getPlayer() {
        return player;
    }

    /**
     * This will set the player data.
     *
     * @param player This is the new player data.
     */
    public void setPlayer(Player player) {
        this.player = player;
    }

    /**
     * This will return the name of the current town.
     *
     * @return This is the name of the current town.
     */
    public String getCurrentTown() {
        return currentTown;
    }

    /**
     * This will set the name of the current town.
     *
     * @param currentTown This is the new name of the current town.
     */
    public void setCurrentTown(String currentTown) {
        this.currentTown = currentTown;
    }
}
