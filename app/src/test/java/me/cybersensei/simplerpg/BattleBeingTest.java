package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.BattleBeing;
import me.cybersensei.simplerpg.lib.JSONSerializable;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.Locale;

/**
 * Unit tests for BattleBeing.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.BattleBeing
 */
public class BattleBeingTest {
    // The battle being name
    private static final String NAME = "Name";

    // The battle being element
    private static final String ELEMENT = "Normal";

    // The battle being level
    private static final int LEVEL = 1;

    // The battle being experience
    private static final int EXPERIENCE = 50;

    // The battle being gold
    private static final int GOLD = 2;

    // The battle being strength
    private static final int STRENGTH = 3;

    // The battle being dexterity
    private static final int DEXTERITY = 4;

    // The battle being constitution
    private static final int CONSTITUTION = 5;

    // The battle being agility
    private static final int AGILITY = 6;

    // The battle being damage
    private static final int DAMAGE = 7;

    // This is the primary battle being for testing with default values
    private BattleBeing being;

    @Before
    public void setUp() {
        this.being = new BattleBeing(NAME, ELEMENT, LEVEL, EXPERIENCE, GOLD, STRENGTH, DEXTERITY, CONSTITUTION, AGILITY,
                DAMAGE);
    }

    @Test
    public void testToString() {
        Assert.assertEquals("Checking String rep of being",
                String.format(Locale.US,
                        "%s type:%s level:%d exp:%d gold:%d strength:%d dexterity:%d constitution:%d agility:%d damage:%d",
                        NAME, ELEMENT, LEVEL, EXPERIENCE, GOLD, STRENGTH, DEXTERITY, CONSTITUTION, AGILITY, DAMAGE),
                this.being.toString());
    }

    @Test
    public void testInitialValues() {
        Assert.assertEquals("Checking name", NAME, this.being.getName());
        Assert.assertEquals("Checking element", ELEMENT, this.being.getElement());
        Assert.assertEquals("Checking level", LEVEL, this.being.getLevel());
        Assert.assertEquals("Checking exp", EXPERIENCE, this.being.getExp());
        Assert.assertEquals("Checking gold", GOLD, this.being.getGold());
        Assert.assertEquals("Checking health", 0, this.being.getHp());
        Assert.assertEquals("Checking strength", STRENGTH, this.being.getStrength());
        Assert.assertEquals("Checking dexterity", DEXTERITY, this.being.getDexterity());
        Assert.assertEquals("Checking constitution", CONSTITUTION, this.being.getConstitution());
        Assert.assertEquals("Checking agility", AGILITY, this.being.getAgility());
        Assert.assertEquals("Checking damage", DAMAGE, this.being.getDamage());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testFromJsonPreStats() throws JSONException {
        final String NAME = "Bob";
        final String ELEMENT = "Fire";
        final int HP = 3;
        final int MAX_HP = 3;
        final int ATTACK = 1;
        final int DEFENSE = 2;
        final int SPEED = 4;
        final int DAMAGE = 9;

        this.being.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(JSONSerializable.ELEMENTAL_TYPE, ELEMENT)
                .put(JSONSerializable.LEVEL, LEVEL)
                .put(JSONSerializable.EXP, EXPERIENCE)
                .put(JSONSerializable.GOLD, GOLD)
                .put(JSONSerializable.HP, HP)
                .put(JSONSerializable.MAX_HP, MAX_HP)
                .put(JSONSerializable.ATTACK, ATTACK)
                .put(JSONSerializable.DEFENSE, DEFENSE)
                .put(JSONSerializable.SPEED, SPEED)
                .put(JSONSerializable.DAMAGE, DAMAGE));

        Assert.assertEquals("Checking name after created from json", NAME, this.being.getName());
        Assert.assertEquals("Checking element after created from json", ELEMENT, this.being.getElement());
        Assert.assertEquals("Checking level after created from json", LEVEL, this.being.getLevel());
        Assert.assertEquals("Checking exp after created from json", EXPERIENCE, this.being.getExp());
        Assert.assertEquals("Checking gold after created from json", GOLD, this.being.getGold());
        Assert.assertEquals("Checking health after created from json", HP, this.being.getHp());
        Assert.assertEquals("Checking strength after created from json", 1, this.being.getStrength());
        Assert.assertEquals("Checking dexterity after created from json", 2, this.being.getDexterity());
        Assert.assertEquals("Checking constitution after created from json", 2, this.being.getConstitution());
        Assert.assertEquals("Checking agility after created from json", 4, this.being.getAgility());
        Assert.assertEquals("Checking damage after created from json", DAMAGE, this.being.getDamage());
    }

    @Test
    public void testFromJSON() throws JSONException {
        final String NAME = "Bob";
        final String ELEMENT = "Fire";
        final int EXP = 5;
        final int HP = 3;
        final int GOLD = 3;
        final int STRENGTH = 1;
        final int DEXTERITY = 2;
        final int CONSTITUTION = 3;
        final int AGILITY = 4;
        final int DAMAGE = 6;

        this.being.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(JSONSerializable.ELEMENTAL_TYPE, ELEMENT)
                .put(JSONSerializable.LEVEL, LEVEL)
                .put(JSONSerializable.EXP, EXP)
                .put(JSONSerializable.GOLD, GOLD)
                .put(JSONSerializable.HP, HP)
                .put(JSONSerializable.STRENGTH, STRENGTH)
                .put(JSONSerializable.DEXTERITY, DEXTERITY)
                .put(JSONSerializable.CONSTITUTION, CONSTITUTION)
                .put(JSONSerializable.AGILITY, AGILITY)
                .put(JSONSerializable.DAMAGE, DAMAGE)
        );

        Assert.assertEquals("Checking name after created from json", NAME, this.being.getName());
        Assert.assertEquals("Checking element after created from json", ELEMENT, this.being.getElement());
        Assert.assertEquals("Checking level after created from json", LEVEL, this.being.getLevel());
        Assert.assertEquals("Checking exp after created from json", EXP, this.being.getExp());
        Assert.assertEquals("Checking gold after created from json", GOLD, this.being.getGold());
        Assert.assertEquals("Checking health after created from json", HP, this.being.getHp());
        Assert.assertEquals("Checking strength after created from json", STRENGTH, this.being.getStrength());
        Assert.assertEquals("Checking dexterity after created from json", DEXTERITY, this.being.getDexterity());
        Assert.assertEquals("Checking constitution after created from json", CONSTITUTION, this.being.getConstitution());
        Assert.assertEquals("Checking agility after created from json", AGILITY, this.being.getAgility());
        Assert.assertEquals("Checking damage after created from json", DAMAGE, this.being.getDamage());
    }

    @Test
    public void testFromJsonOldElementalTypeField() throws JSONException {
        final String NAME = "Bob";
        final String ELEMENT = "Fire";
        final int EXP = 5;
        final int HP = 3;
        final int GOLD = 3;
        final int STRENGTH = 1;
        final int DEXTERITY = 2;
        final int CONSTITUTION = 3;
        final int AGILITY = 4;
        final int DAMAGE = 6;

        this.being.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(BattleBeing.ELEMENTAL_TYPE_LEGACY, ELEMENT)
                .put(JSONSerializable.LEVEL, LEVEL)
                .put(JSONSerializable.EXP, EXP)
                .put(JSONSerializable.GOLD, GOLD)
                .put(JSONSerializable.HP, HP)
                .put(JSONSerializable.STRENGTH, STRENGTH)
                .put(JSONSerializable.DEXTERITY, DEXTERITY)
                .put(JSONSerializable.CONSTITUTION, CONSTITUTION)
                .put(JSONSerializable.AGILITY, AGILITY)
                .put(JSONSerializable.DAMAGE, DAMAGE)
        );

        Assert.assertEquals("Checking name after created from json", NAME, this.being.getName());
        Assert.assertEquals("Checking element after created from json", ELEMENT, this.being.getElement());
        Assert.assertEquals("Checking level after created from json", LEVEL, this.being.getLevel());
        Assert.assertEquals("Checking exp after created from json", EXP, this.being.getExp());
        Assert.assertEquals("Checking gold after created from json", GOLD, this.being.getGold());
        Assert.assertEquals("Checking health after created from json", HP, this.being.getHp());
        Assert.assertEquals("Checking strength after created from json", STRENGTH, this.being.getStrength());
        Assert.assertEquals("Checking dexterity after created from json", DEXTERITY, this.being.getDexterity());
        Assert.assertEquals("Checking constitution after created from json", CONSTITUTION, this.being.getConstitution());
        Assert.assertEquals("Checking agility after created from json", AGILITY, this.being.getAgility());
        Assert.assertEquals("Checking damage after created from json", DAMAGE, this.being.getDamage());
    }

    @Test
    public void testFromJsonWithoutElement() throws JSONException {
        final String NAME = "Bob";
        final int EXP = 5;
        final int HP = 3;
        final int GOLD = 3;
        final int STRENGTH = 1;
        final int DEXTERITY = 2;
        final int CONSTITUTION = 3;
        final int AGILITY = 4;
        final int DAMAGE = 6;

        this.being.fromJSON(new JSONObject()
                .put(JSONSerializable.NAME, NAME)
                .put(JSONSerializable.LEVEL, LEVEL)
                .put(JSONSerializable.EXP, EXP)
                .put(JSONSerializable.GOLD, GOLD)
                .put(JSONSerializable.HP, HP)
                .put(JSONSerializable.STRENGTH, STRENGTH)
                .put(JSONSerializable.DEXTERITY, DEXTERITY)
                .put(JSONSerializable.CONSTITUTION, CONSTITUTION)
                .put(JSONSerializable.AGILITY, AGILITY)
                .put(JSONSerializable.DAMAGE, DAMAGE)
        );

        Assert.assertEquals("Checking name after created from json", NAME, this.being.getName());
        Assert.assertEquals("Checking element after created from json", "Normal", this.being.getElement());
        Assert.assertEquals("Checking level after created from json", LEVEL, this.being.getLevel());
        Assert.assertEquals("Checking exp after created from json", EXP, this.being.getExp());
        Assert.assertEquals("Checking gold after created from json", GOLD, this.being.getGold());
        Assert.assertEquals("Checking health after created from json", HP, this.being.getHp());
        Assert.assertEquals("Checking strength after created from json", STRENGTH, this.being.getStrength());
        Assert.assertEquals("Checking dexterity after created from json", DEXTERITY, this.being.getDexterity());
        Assert.assertEquals("Checking constitution after created from json", CONSTITUTION, this.being.getConstitution());
        Assert.assertEquals("Checking agility after created from json", AGILITY, this.being.getAgility());
        Assert.assertEquals("Checking damage after created from json", DAMAGE, this.being.getDamage());
    }

    @Test
    public void testToJSON() throws JSONException {
        final int STRENGTH = 1;
        final int DEXTERITY = 2;
        final int CONSTITUTION = 3;
        final int AGILITY = 4;

        this.being.setStrength(STRENGTH);
        this.being.setDexterity(DEXTERITY);
        this.being.setConstitution(CONSTITUTION);
        this.being.setAgility(AGILITY);

        JSONObject jSONObject = this.being.toJSON();

        Assert.assertNotNull("Making sure json is not null", jSONObject);
        Assert.assertEquals("Checking name in json", this.being.getName(), jSONObject.getString(JSONSerializable.NAME));
        Assert.assertEquals("Checking element in json", this.being.getElement(), jSONObject.getString(JSONSerializable.ELEMENTAL_TYPE));
        Assert.assertEquals("Checking level in json", this.being.getLevel(), jSONObject.getInt(JSONSerializable.LEVEL));
        Assert.assertEquals("Checking gold in json", this.being.getGold(), jSONObject.getInt(JSONSerializable.GOLD));
        Assert.assertEquals("Checking hp in json", this.being.getHp(), jSONObject.getInt(JSONSerializable.HP));
        Assert.assertEquals("Checking value of strength in json", STRENGTH, jSONObject.getInt(JSONSerializable.STRENGTH));
        Assert.assertEquals("Checking value of dexterity int json", DEXTERITY, jSONObject.getInt(JSONSerializable.DEXTERITY));
        Assert.assertEquals("Checking value of constitution in json", CONSTITUTION, jSONObject.getInt(JSONSerializable.CONSTITUTION));
        Assert.assertEquals("Checking value of agility in json", AGILITY, jSONObject.getInt(JSONSerializable.AGILITY));
        Assert.assertEquals("Checking damage in json", this.being.getDamage(), jSONObject.getInt(JSONSerializable.DAMAGE));
    }

    @Test
    public void testSetElement() {
        this.being.setElement("Fire");
        Assert.assertEquals("Checking element after set", "Fire", this.being.getElement());
    }

    @Test
    public void testSetLevel() {
        this.being.setLevel(10);
        Assert.assertEquals("Checking level after set", 10, this.being.getLevel());
    }

    @Test
    public void testSetExp() {
        this.being.setExp(10);
        Assert.assertEquals("Checking exp after set", 10, this.being.getExp());
    }

    @Test
    public void testStrength() {
        this.being.setStrength(5);
        Assert.assertEquals("Checking strength after set to 5", 5, this.being.getStrength());
    }

    @Test
    public void testDexterity() {
        this.being.setDexterity(4);
        Assert.assertEquals("Checking dexterity after set to 4", 4, this.being.getDexterity());
    }

    @Test
    public void testConstitution() {
        this.being.setConstitution(6);
        Assert.assertEquals("Checking constitution after set to 6", 6, this.being.getConstitution());
    }

    @Test
    public void testAgility() {
        this.being.setAgility(5);
        Assert.assertEquals("Checking agility after set to 5", 5, this.being.getAgility());
    }

    @Test
    public void testSetDamage() {
        this.being.setDamage(10);
        Assert.assertEquals("Checking damage after set", 10, this.being.getDamage());
    }

    @Test
    public void testSetGold() {
        this.being.setGold(10);
        Assert.assertEquals("Checking gold after set", 10, this.being.getGold());
    }

    @Test
    public void testMaxHp() {
        this.being.setConstitution(3);
        Assert.assertEquals("Checking max hp calculation (1)", 4, this.being.getMaxHp());
        this.being.setConstitution(6);
        Assert.assertEquals("Checking max hp calculations (3)", 8, this.being.getMaxHp());
        this.being.setConstitution(9);
        Assert.assertEquals("Checking max hp calculations (3)", 12, this.being.getMaxHp());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testSetMaxHp() {
        this.being.setMaxHp(10);
        Assert.assertEquals("Checking constitution after set to 10", 8, this.being.getConstitution());
    }

    @Test
    public void testAttack() {
        this.being.setStrength(1);
        Assert.assertEquals("Checking attack calculation s(1)", 1, this.being.getAttack());
        this.being.setStrength(4);
        Assert.assertEquals("Checking attack calculation s(4)", 2, this.being.getAttack());
        this.being.setStrength(9);
        Assert.assertEquals("Checking attack calculation s(9)", 5, this.being.getAttack());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testSetAttack() {
        this.being.setAttack(10);
        Assert.assertEquals("Checking strength after set", 10, this.being.getStrength());
    }

    @Test
    public void testDefense() {
        this.being.setDexterity(1);
        Assert.assertEquals("Checking defense calculation d(1)", 0, this.being.getDefense());
        this.being.setDexterity(4);
        Assert.assertEquals("Checking defense calculation d(4)", 3, this.being.getDefense());
        this.being.setDexterity(9);
        Assert.assertEquals("Checking defense calculation d(9)", 6, this.being.getDefense());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testSetDefense() {
        this.being.setDefense(10);
        Assert.assertEquals("Checking defense after set", 10, this.being.getDexterity());
    }

    @Test
    public void testSpeed() {
        this.being.setAgility(1);
        Assert.assertEquals("Checking agility calculation a(1)", 1, this.being.getSpeed());
        this.being.setAgility(4);
        Assert.assertEquals("Checking agility calculation a(4)", 3, this.being.getSpeed());
        this.being.setAgility(9);
        Assert.assertEquals("Checking agility calculation a(9)", 6, this.being.getSpeed());
    }

    @SuppressWarnings("deprecation")
    @Test
    public void testSetSpeed() {
        this.being.setSpeed(10);
        Assert.assertEquals("Checking speed after set", 10, this.being.getAgility());
    }

    @Test
    public void testDamage() {
        this.being.fullHeal();
        this.being.damage(1);
        Assert.assertEquals("Checking hp after damage", this.being.getMaxHp() - 1, this.being.getHp());
        this.being.damage(-1);
        Assert.assertEquals("Checking hp after attempted negative damage", this.being.getMaxHp() - 1, this.being.getHp());
    }

    @Test
    public void testHeal() {
        this.being.heal(5);
        Assert.assertEquals("Checking health after heal 5", 5, this.being.getHp());
        this.being.heal(-1);
        Assert.assertEquals("Checking health after attempted -1 heal", 5, this.being.getHp());
        this.being.heal(999);
        Assert.assertEquals("Checking health after over heal", this.being.getMaxHp(), this.being.getHp());
    }

    @Test
    public void testFullHeal() {
        this.being.fullHeal();
        Assert.assertEquals("Checking health after full heal", this.being.getMaxHp(), this.being.getHp());
    }
}
