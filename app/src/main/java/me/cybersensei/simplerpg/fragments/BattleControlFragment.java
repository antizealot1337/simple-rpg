package me.cybersensei.simplerpg.fragments;

import android.app.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import me.cybersensei.simplerpg.BuildConfig;
import me.cybersensei.simplerpg.R;

/**
 * This fragment contains all the battle controls. Any Activity this fragment attaches to should implement the
 * BattleControlListener.
 *
 * @author Christpher Pridgen
 * @see BattleControlListener
 */
public class BattleControlFragment extends Fragment implements View.OnClickListener {
    // The tag for logging
    private final static String TAG = BattleControlFragment.class.getSimpleName();

    // The tag for the mode
    private static final String BATTLE_MODE = "battleMode";

    // The attack Button
    private Button attack;

    // The completed Button
    private Button completed;

    // The flee Button
    private Button flee;

    // The listener for the fragment
    private BattleControlListener listener;

    // The current mode
    private Mode mode = Mode.BATTLING;

    @Override
    public void onAttach(Context context) {
        // Cast the context to the BattleControlListener
        this.listener = (BattleControlListener) context;
        super.onAttach(context);
    }

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        // Inflate the view
        View view = layoutInflater.inflate(R.layout.fragment_battle_control, viewGroup, false);

        // Find specific children
        this.attack = view.findViewById(R.id.b_attack);
        this.flee = view.findViewById(R.id.b_flee);
        this.completed = view.findViewById(R.id.b_completed);

        // Set the listeners
        this.attack.setOnClickListener(this);
        this.flee.setOnClickListener(this);
        this.completed.setOnClickListener(this);

        if (bundle != null && bundle.containsKey(BATTLE_MODE)) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Loading mode from state");
            this.setMode(Mode.valueOf(bundle.getString(BATTLE_MODE)));
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (BuildConfig.DEBUG) Log.d(TAG, "Saving battle mode");
        outState.putString(BATTLE_MODE, this.mode.name());
    }

    @Override
    public void onDetach() {
        super.onDetach();
        this.listener = null; // Set the listener to null
    }

    @Override
    public void onClick(View view) {
        // Act based on the view id
        switch (view.getId()) {
            case R.id.b_attack:
                this.listener.attack();
                break;
            case R.id.b_flee:
                this.listener.flee();
                break;
            case R.id.b_completed:
                // Act based on the current mode also
                switch (this.mode) {
                    case BATTLING:
                    case WON:
                        this.listener.afterWin();
                        break;
                    case DEFEATED:
                        this.listener.afterDefeat();
                        break;
                    case PLAYER_FLED:
                    case MONSTER_FLED:
                        this.listener.afterFlee();
                        break;
                    default:
                        return;
                }
                break;
            default:
                if (BuildConfig.DEBUG) Log.d(TAG, "Unhandled click");
                break;
        }
    }

    /**
     * Changes whether the attack and flee buttons are enabled or disabled.
     *
     * @param enable If true the buttons are enabled otherwise they are disabled.
     */
    public void setButtonsEnabled(boolean enable) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Setting buttons enabled to " + enable);
        this.attack.setEnabled(enable);
        this.flee.setEnabled(enable);
    }

    /**
     * Changes the mode of the fragment. The modes WON, DEFEATED, MONSTER_FLED, and PLAYER_FLED will deactivate the
     * battle controls and set a button visible and set the text to the button. Any other modes don't have any change to
     * the layout.
     *
     * @param mode The new mode for the fragment.
     * @see me.cybersensei.simplerpg.fragments.BattleControlFragment.Mode
     */
    public void setMode(Mode mode) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Setting mode to " + mode);
        this.mode = mode;
        int id;
        switch (mode) {
            case WON:
                id = R.string.won;
                break;
            case DEFEATED:
                id = R.string.defeat;
                break;
            case MONSTER_FLED:
                id = R.string.enemy_fled;
                break;
            case PLAYER_FLED:
                id = R.string.player_fled;
                break;
            default:
                // Do nothing
                return;
        }
        deactivateBattleControls();
        activateCompleted(id);
    }

    /**
     * This sets the completed Button to visible and sets the text to the provided resource id.
     *
     * @param id This is a resource id.
     */
    private void activateCompleted(int id) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Enable completed buttons and set text");
        this.completed.setVisibility(View.VISIBLE);
        this.completed.setText(id);
    }

    /**
     * This sets the button controls (the attack and flee buttons) to View.GONE.
     */
    private void deactivateBattleControls() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Deactivating battle controls");
        this.attack.setVisibility(View.GONE);
        this.flee.setVisibility(View.GONE);
    }

    /**
     * This is the interface so that the BattleControlFragment can communicate battle actions to the parent Activity.
     * Any Activity that houses a BattleControlFragment must implement this interface.
     */
    public interface BattleControlListener {
        /**
         * This is called after the player has been defeated.
         */
        void afterDefeat();

        /**
         * This is called after someone fled the battle. The identity of the coward
         * is not indicated.
         */
        void afterFlee();

        /**
         * This is called after the player has vanquished an enemy.
         */
        void afterWin();

        /**
         * This indicates the player wishes to attack.
         */
        void attack();

        /**
         * This indicates the player wishes to flee.
         */
        void flee();
    }

    /**
     * These are the various states of the BattleControlFragment.
     */
    public enum Mode {
        BATTLING,
        WON,
        DEFEATED,
        MONSTER_FLED,
        PLAYER_FLED,
    }
}
