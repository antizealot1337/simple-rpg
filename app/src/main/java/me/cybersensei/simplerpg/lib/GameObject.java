package me.cybersensei.simplerpg.lib;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * This is the base class of all the different loadable objects in the game.
 *
 * @author Christopher Pridgen
 */
public abstract class GameObject implements JSONSerializable {
    /**
     * This is the field name for name.
     */
    public static final String NAME = "name";

    /**
     * This is the name.
     */
    protected String name;

    @Override
    public String toString() {
        return this.name;
    }

    /**
     * This will set the values of the GameObject from a JSONObject. All direct children should override this to be
     * able to extract their own values from JSON data. Most should call this method in their own unless a good reason
     * not to do so exists.
     *
     * @param jsonObject This is the JSONObject that contains the data to be loaded.
     * @throws JSONException This will be thrown if there is an error reading from the JSONObject.
     */
    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        this.name = jsonObject.getString(NAME);
    }

    /**
     * This will return a JSONObject with the GameObject data. All direct children should override this to be able to
     * serialize their own values to JSON data. Most should call this method in their own unless a good reason not to do
     * so exists.
     *
     * @return This is the new constructed JSONObject with GameObject data.
     * @throws JSONException This will be thrown if there is an error writing data to the JSONObject.
     */
    @Override
    public JSONObject toJSON() throws JSONException {
        return new JSONObject().put(NAME, this.name);
    }

    /**
     * Get the name.
     *
     * @return This is the value of the name.
     */
    public String getName() {
        return this.name;
    }

    /**
     * Set the name
     *
     * @param name The new value of the name.
     */
    public void setName(String name) {
        this.name = name;
    }
}
