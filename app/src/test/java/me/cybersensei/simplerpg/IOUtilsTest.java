package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.IOUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * Unit tests for IOUtils.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.IOUtils
 */
public class IOUtilsTest {
    @Test
    public void testReadAll() throws IOException {
        final String INPUT = "This is the input";

        String output = IOUtils.readAllString(new ByteArrayInputStream(INPUT.getBytes()));

        Assert.assertEquals("Checking the output", INPUT, output);
    }

    @Test
    public void testReadAllJsonObject() throws JSONException, IOException {
        final String FIELD_NAME = "field";
        final String FIELD_VALUE = "value";

        JSONObject jsonObject = IOUtils.readAllJsonObject(new ByteArrayInputStream(new JSONObject()
                .put(FIELD_NAME, FIELD_VALUE)
                .toString().getBytes()));

        Assert.assertTrue("Checking for the field", jsonObject.has(FIELD_NAME));
        Assert.assertEquals("Checking the field value", FIELD_VALUE, jsonObject.getString(FIELD_NAME));
    }

    @Test
    public void testReadAllJsonArray() throws JSONException, IOException {
        JSONArray jsonArray = IOUtils.readAllJsonArray(new ByteArrayInputStream(new JSONArray()
                .put(0)
                .put(1)
                .put(2)
                .toString().getBytes()));

        Assert.assertEquals("Checking json array length", 3, jsonArray.length());
        Assert.assertEquals("Checking first value", 0, jsonArray.getInt(0));
        Assert.assertEquals("Checking second value", 1, jsonArray.getInt(1));
        Assert.assertEquals("Checking third value", 2, jsonArray.getInt(2));
    }
}
