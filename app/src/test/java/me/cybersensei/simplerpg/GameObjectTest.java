package me.cybersensei.simplerpg;

import me.cybersensei.simplerpg.lib.GameObject;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 * Unit tests for GameObject.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.GameObject
 */
public class GameObjectTest {
    // This is the name for testing
    private static final String NAME = "GameObject";

    // This is the primary game object for testing with default values
    private GameObject go;

    @Before
    public void setUp() {
        this.go = new GameObject() {
            public void fromJSON(JSONObject jsonObject) throws JSONException {
                super.fromJSON(jsonObject);
            }

            public JSONObject toJSON() throws JSONException {
                return super.toJSON();
            }
        };
    }

    @Test
    public void testName() {
        Assert.assertNull("Making sure name is null", this.go.getName());
        this.go.setName(NAME);
        Assert.assertEquals("Checking name setter/getter", NAME, this.go.getName());
    }

    @Test
    public void testFromJSON() throws JSONException {
        final String NEW_NAME = "New Name";

        this.go.fromJSON(new JSONObject()
                .put(GameObject.NAME, NEW_NAME)
        );

        Assert.assertEquals("Checking the name", NEW_NAME, this.go.getName());
    }

    @Test
    public void testToJSON() throws JSONException {
        this.go.setName(NAME);
        JSONObject jsonObject = this.go.toJSON();

        Assert.assertNotNull("Checking the json object", jsonObject);
        Assert.assertTrue("Checking for name in json", jsonObject.has(GameObject.NAME));
        Assert.assertEquals("Checking the name value in the json object", NAME, jsonObject.getString(GameObject.NAME));
    }
}
