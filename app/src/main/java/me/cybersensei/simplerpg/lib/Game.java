package me.cybersensei.simplerpg.lib;

import org.json.JSONException;

import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.util.List;
import java.util.Random;

/**
 * This encapsulates the game data and a few functions required for playing the game. This is also responsible for
 * saving and loading game data and save data.
 *
 * @author Christopher Pridgen
 */
public class Game {
    /**
     * This is the game's source of random
     */
    public static final Random random = new Random();

    // This is the world data
    private final WorldData worldData;

    // This is the save data
    private final SaveData saveData;

    // This is the cached value of the current town to avoid too many lookups of the world data and save data.
    private Town cachedTown;

    /**
     * This is the primary constructor. It accepts data for the world as well as player info. The player info can be
     * null for new games. If null is passed to the saveData parameter then a new object will be created.
     * It is important to note that the new object will lack Player data and thus the game will not be ready to play.
     *
     * @param worldData This is the static game data.
     * @param saveData  This is the save data from a previous game. If this is null then it is assumed a new game is
     *                  starting.
     */
    public Game(WorldData worldData, SaveData saveData) {
        this.worldData = worldData;
        if (saveData == null) {
            saveData = new SaveData(null, this.worldData.getStartingTown());
        }
        this.saveData = saveData;
        this.cachedTown = this.worldData.findTown(this.saveData.getCurrentTown());
    }

    @Override
    public String toString() {
        return "Game{" +
                "worldData=" + worldData +
                ", saveData=" + saveData +
                '}';
    }

    /**
     * This will return the player.
     *
     * @return This is the player data.
     */
    public Player getPlayer() {
        return this.saveData.getPlayer();
    }

    /**
     * This will set the player.
     *
     * @param player This is the new player info.
     */
    public void setPlayer(Player player) {
        this.saveData.setPlayer(player);
    }

    /**
     * This will return the current town.
     *
     * @return This is the current town.
     */
    public Town getCurrentTown() {
        return this.cachedTown;
    }

    /**
     * This will set the current town.
     *
     * @param town This is the new current town.
     */
    public void setCurrentTown(Town town) {
        this.saveData.setCurrentTown(town.getName());
        this.cachedTown = town;
    }

    /**
     * This will attempt to find an element by its name.
     *
     * @param name The name of the element to find.
     * @return This will either be the element requested or null if it does not exist.
     */
    public ElementalType findElementByName(String name) {
        return this.worldData.findElement(name);
    }

    /**
     * This will attempt to find a creature by its name.
     *
     * @param name The name of the creature to find.
     * @return This will either be the creature requested or null if it does not exist.
     */
    public Creature findCreatureByName(String name) {
        return this.worldData.findCreature(name);
    }

    /**
     * This will attempt to find a town by its name.
     *
     * @param name The name of the town to find.
     * @return This will either be the town requested or null if it does not exist.
     */
    public Town findTownByName(String name) {
        return this.worldData.findTown(name);
    }

    /**
     * This checks if the player can stay at the inn. It only checks based on the gold amount.
     *
     * @return This will return true if the player can stay at the inn.
     */
    public boolean canStayAtInn() {
        return this.saveData.getPlayer().hasEnoughGold(this.cachedTown.getInnCost());
    }

    /**
     * This will deduct the price of the inn from the player's gold and fully heal the player. No checks are performed
     * so this should be proceeded by a call to canStayAtInn.
     */
    public void stayAtInn() {
        this.saveData.getPlayer().chargeGold(this.cachedTown.getInnCost());
        this.saveData.getPlayer().fullHeal();
    }

    /**
     * This checks if there is an encounter in the current town's field.
     *
     * @return The name of the creature encountered or null if there was no encounter.
     */
    public String checkForEncounter() {
        if (Game.random.nextInt(3) == 2 && this.cachedTown.hasEncounters()) {
            List<String> creaturesInField = this.cachedTown.getEncounters();
            return creaturesInField.get(Game.random.nextInt(creaturesInField.size()));
        }
        return null;
    }

    /**
     * This saves the game data to an output stream. It does so by serializing the current game with toJSON and
     * converting that into a String. The resulting String is then written to the output stream.
     *
     * @param outputStream This is the output stream for the serialized saved game data.
     * @throws JSONException This is an exception thrown when there is a problem serializing the data.
     * @throws IOException   This is an exception thrown when there is a problem writing the serialized data.
     */
    public void save(OutputStream outputStream) throws JSONException, IOException {
        OutputStreamWriter outputStreamWriter = new OutputStreamWriter(outputStream);
        outputStreamWriter.write(this.saveData.toJSON().toString(2));
        outputStreamWriter.flush();
    }
}
