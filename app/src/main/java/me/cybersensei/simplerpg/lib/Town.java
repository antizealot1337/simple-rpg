package me.cybersensei.simplerpg.lib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * This is a location in the game. It has a field with creatures in it and an inn where the player can stay. It also has
 * roads to other towns the player may visit.
 *
 * @author Christopher Pridgen
 * @see me.cybersensei.simplerpg.lib.Road
 * @see me.cybersensei.simplerpg.lib.Creature
 */
public class Town extends GameObject {
    /**
     * This is the field name for an inn name.
     */
    public static final String INN_NAME = "innName";

    /**
     * This is the field name for inn cost.
     */
    public static final String INN_COST = "innCost";

    /**
     * This is the field name for field creatures.
     */
    public static final String FIELD_CREATURES = "fieldCreatures";

    /**
     * This is the field name for roads.
     */
    public static final String ROADS = "roads";

    // These are the names of the creatures in the field.
    private final ArrayList<String> creaturesInField = new ArrayList<>();

    // These are the roads to other places.
    private final ArrayList<Road> roads = new ArrayList<>();

    // This is the name of the inn if there is an inn
    private String innName = "Inn";

    // This is the cost of the inn
    private int innCost;

    /**
     * This is the primary constructor for a town.
     *
     * @param name    This is the name of the town.
     * @param innName This is the name of the inn. If null is provided then the default name will be used.
     * @param innCost This is the cost of the inn.
     */
    public Town(String name, String innName, int innCost) {
        this.name = name;
        if (innName != null)
            this.innName = innName;
        this.innCost = innCost;
    }

    /**
     * This constructor creates a Town from a JSONObject. It is similar to creating a new object and calling fromJSON
     * but is safer.
     *
     * @param jsonObject This is the JSONObject that holds the Town data.
     * @throws JSONException This is an exception thrown by fromJSON.
     */
    public Town(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }

    @Override
    public String toString() {
        return String.format(Locale.US,
                "%s inn:%s inn cost:%d creatures:%s roads:%s", this.name, this.innName,
                this.innCost, creaturesString(), travelString());
    }

    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);

        this.innName = jsonObject.getString(INN_NAME);
        this.innCost = jsonObject.getInt(INN_COST);

        JSONArray jsonArray = jsonObject.getJSONArray(FIELD_CREATURES);
        for (int i = 0; i < jsonArray.length(); i++) {
            this.creaturesInField.add(jsonArray.getString(i));
        }

        jsonArray = jsonObject.getJSONArray(ROADS);
        for (int i = 0; i < jsonArray.length(); i++) {
            this.roads.add(new Road(jsonArray.getJSONObject(i)));
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();
        jsonObject.put(INN_NAME, this.innName);
        jsonObject.put(INN_COST, this.innCost);

        JSONArray roadsJArray = new JSONArray();
        for (Road road : this.roads) {
            roadsJArray.put(road.toJSON());
        }
        jsonObject.put(ROADS, roadsJArray);

        JSONArray creaturesJArray = new JSONArray();
        for (String s : this.creaturesInField) {
            creaturesJArray.put(s);
        }
        jsonObject.put(FIELD_CREATURES, creaturesJArray);

        return jsonObject;
    }

    /**
     * Get the creatures in the field.
     *
     * @return The list of creature names.
     */
    public List<String> getEncounters() {
        return this.creaturesInField;
    }

    /**
     * Get the roads to other places.
     *
     * @return This is the list of roads.
     */
    public List<Road> getRoads() {
        return this.roads;
    }

    /**
     * Get the inn name.
     *
     * @return This is the name of the inn.
     */
    public String getInnName() {
        return this.innName;
    }

    /**
     * Set the inn name.
     *
     * @param name This is the new name of the inn.
     */
    public void setInnName(String name) {
        this.innName = name;
    }

    /**
     * Get the inn cost.
     *
     * @return This is the cost of the inn.
     */
    public int getInnCost() {
        return this.innCost;
    }

    /**
     * Set the inn cost.
     *
     * @param cost This is the new cost of the inn.
     */
    public void setInnCost(int cost) {
        this.innCost = cost;
    }

    /**
     * Add a creature to the field.
     *
     * @param name This is the name of the creature to add to the field.
     */
    public void addCreature(String name) {
        this.creaturesInField.add(name);
    }

    /**
     * Add a new road. This constructs a Road.
     *
     * @param name    This is the name of the road.
     * @param enabled This determines if the new road is enabled or not.
     * @see me.cybersensei.simplerpg.lib.Road
     */
    public void addRoad(String name, boolean enabled) {
        this.roads.add(new Road(name, enabled));
    }

    /**
     * This will check if there are any encounters for this Town. I checks to see if the encounters list is empty.
     * @return this will be true if there are encounters available and false if not.
     */
    public boolean hasEncounters() {
        return !this.creaturesInField.isEmpty();
    }

    /**
     * Create a string with all the creature names.
     *
     * @return A string with all the names of the creatures in the field.
     */
    private String creaturesString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.creaturesInField.size(); i++) {
            stringBuilder.append(this.creaturesInField.get(i));
            if (i != this.creaturesInField.size() - 1) {
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }

    /**
     * This creates a string with all the road names.
     *
     * @return A string with all the names of the roads.
     */
    private String travelString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < this.roads.size(); i++) {
            stringBuilder.append(this.roads.get(i));
            if (i != this.roads.size() - 1) {
                stringBuilder.append(", ");
            }
        }
        return stringBuilder.toString();
    }
}
