package me.cybersensei.simplerpg.lib;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class represents elements that monsters and players can exhibit that affects the damage they do and receive. The
 * element has strengths and weaknesses. If one element is strong against another then the damage is increased. If one
 * element is weak against another then damage is lowered.
 *
 * @author Christopher Pridgen
 */
public class ElementalType extends GameObject {
    /**
     * This is the field name for strengths.
     */
    public static final String STRENGTHS = "strengths";

    /**
     * This is the field name for weaknesses.
     */
    public static final String WEAKNESSES = "weaknesses";

    // These are the names of the other elements this one is strong against.
    private final ArrayList<String> strengths = new ArrayList<>();

    // These are the names of other elements this one is weak against.
    private final ArrayList<String> weaknesses = new ArrayList<>();

    /**
     * This is the main constructor. It only sets the name since the strengths and weaknesses are already initialized.
     *
     * @param name This is the element name.
     */
    public ElementalType(String name) {
        this.name = name;
    }

    /**
     * This constructor creates an Element from a JSONObject. It is similar to creating a new object and calling
     * fromJSON but is safer.
     *
     * @param jsonObject This is the JSONObject that holds the Element data.
     * @throws JSONException This is an exception thrown by fromJSON.
     */
    public ElementalType(JSONObject jsonObject) throws JSONException {
        fromJSON(jsonObject);
    }

    @Override
    public String toString() {
        return this.name + " Strengths: " + this.strengths + " Weaknesses: " + this.weaknesses;
    }

    @Override
    public void fromJSON(JSONObject jsonObject) throws JSONException {
        super.fromJSON(jsonObject);

        JSONArray jsonArray = jsonObject.getJSONArray(STRENGTHS);
        for (int i = 0; i < jsonArray.length(); i++) {
            this.strengths.add(jsonArray.getString(i));
        }

        jsonArray = jsonObject.getJSONArray(WEAKNESSES);
        for (int i = 0; i < jsonArray.length(); i++) {
            this.weaknesses.add(jsonArray.getString(i));
        }
    }

    @Override
    public JSONObject toJSON() throws JSONException {
        JSONObject jsonObject = super.toJSON();

        JSONArray jsonArray = new JSONArray();
        for (String strength : this.strengths) {
            jsonArray.put(strength);
        }
        jsonObject.put(STRENGTHS, jsonArray);

        jsonArray = new JSONArray();
        for (String weakness : this.weaknesses) {
            jsonArray.put(weakness);
        }
        jsonObject.put(WEAKNESSES, jsonArray);
        return jsonObject;
    }

    /**
     * This returns the element strengths.
     *
     * @return Ths list of element strengths.
     */
    public List<String> getStrengths() {
        return this.strengths;
    }

    /**
     * This checks to see if the Element is strong against another element. It checks to see if the name is contained in
     * strengths field.
     *
     * @param name This is the name of the other element.
     * @return This will be true if the name is in the strengths field and false otherwise.
     */
    public boolean isStrongAgainst(String name) {
        return this.strengths.contains(name);
    }

    /**
     * This adds an Element name to the strengths field. If the element already exists it will not be duplicated.
     *
     * @param name This is the name of the other element.
     * @return This returns true if the element was not present in the strengths field and was added. Otherwise it will
     * return false.
     */
    public boolean addStrength(String name) {
        if (this.strengths.contains(name))
            return false;
        this.strengths.add(name);
        return true;
    }

    /**
     * This returns the element weaknesses.
     *
     * @return Ths list of element weaknesses.
     */
    public List<String> getWeaknesses() {
        return this.weaknesses;
    }

    /**
     * This checks to see if the Element is weak against another element. It checks to see if the name is contained in
     * weaknesses field.
     *
     * @param name This is the name of the other element.
     * @return This will be true if the name is in the weaknesses field and false otherwise.
     */
    public boolean isWeakAgainst(String name) {
        return this.weaknesses.contains(name);
    }

    /**
     * This adds an Element name to the weaknesses field. If the element already exists it will not be duplicated.
     *
     * @param name This is the name of the other element.
     * @return This returns true if the element was not present in the weaknesses field and was added. Otherwise it will
     * return false.
     */
    public boolean addWeakness(String name) {
        if (this.weaknesses.contains(name))
            return false;
        this.weaknesses.add(name);
        return true;
    }

    /**
     * This is used to calculate the damage multiplier compared to another elemental. It is assumed the calling element
     * is the "attacking" element, and the provided element is the "defending".
     *
     * @param other The other element to determine the damage multiplier against.
     * @return The damage multiplier.
     */
    public double calculateDamageMultiplier(ElementalType other) {
        if (this.isStrongAgainst(other.getName())) {
            return 1.5;
        }
        if (this.isWeakAgainst(other.getName())) {
            return 0.75;
        }
        return 1.0;
    }
}
