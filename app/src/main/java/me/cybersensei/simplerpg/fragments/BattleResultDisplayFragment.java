package me.cybersensei.simplerpg.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import me.cybersensei.simplerpg.BuildConfig;
import me.cybersensei.simplerpg.R;
import me.cybersensei.simplerpg.lib.BattleBeing;
import me.cybersensei.simplerpg.lib.BattleResult;

import java.util.Locale;

/**
 * This fragment contains information about the battle including the player and opponent health and the battle log.
 *
 * @author Christopher Pridgen
 */
public class BattleResultDisplayFragment extends Fragment {
    // The tag for logging
    private static final String TAG = BattleResultDisplayFragment.class.getSimpleName();

    // The tag for the battle log
    private static final String BATTLE_LOG = "battleLog";

    // This contains the event log in a textual form
    private final StringBuilder builder = new StringBuilder();

    // The output TextView
    private TextView output;

    @Override
    public View onCreateView(LayoutInflater layoutInflater, ViewGroup viewGroup, Bundle bundle) {
        // Inflate the view
        View view = layoutInflater.inflate(R.layout.fragment_battle_result_display, viewGroup, true);

        // Find specific children
        this.output = view.findViewById(R.id.tv_output);

        if (bundle != null && bundle.containsKey(BATTLE_LOG)) {
            if (BuildConfig.DEBUG) Log.d(TAG, "Loading battle log from bundle state");

            builder.append(bundle.getString(BATTLE_LOG));
            updateOutput();
        }

        return view;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (BuildConfig.DEBUG) Log.d(TAG, "Saving battle log to bundle state");
        outState.putString(BATTLE_LOG, builder.toString());
    }

    /**
     * This is only called at the start of the battle. It adds an message to the log about the opponent that was
     * encountered.
     *
     * @param opponent The opponent that was encountered.
     * @see me.cybersensei.simplerpg.lib.BattleBeing
     */
    public void encountered(BattleBeing opponent) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Informed of encounter with " + opponent.getName());
        this.builder
                .append(String.format(Locale.getDefault(),
                        getResources().getString(R.string.encountered_format_string),
                        opponent.getName()))
                .append('\n');
        updateOutput();
    }

    /**
     * This is called to add the result info to the battle log.
     *
     * @param result The result of some event during the battle.
     * @see me.cybersensei.simplerpg.lib.BattleResult
     */
    public void addResult(BattleResult result) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Adding battle result " + result.getMessage());
        this.builder.append(result.getMessage()).append('\n');
        updateOutput();
    }

    /**
     * Updates the output text with the battle log.
     */
    private void updateOutput() {
        this.output.setText(this.builder.toString());
    }
}
