package me.cybersensei.simplerpg;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import java.util.Locale;

/**
 * This activity is displayed when the player has lost the game. It shows a message to inform the player about the
 * defeat.
 *
 * @author Christopher Pridgen
 */
public class DefeatActivity extends Activity implements View.OnClickListener {
    // The tag for logging
    private static final String TAG = DefeatActivity.class.getSimpleName();

    // Key for storing and retrieving the defeat text
    private static final String DEFEAT_TEXT_KEY = "defeat text";

    // The defeat TextView
    private TextView defeat;

    // The defeat String
    private String defeatText;

    @Override
    protected void onCreate(Bundle bundle) {
        // Initialize the activity
        super.onCreate(bundle);
        setContentView(R.layout.activity_defeat);

        // Find the defeat TextView
        this.defeat = findViewById(R.id.tv_defeat);

        // Format the defeat text
        this.defeatText = String.format(Locale.getDefault(),
                getResources().getString(R.string.defeat_format_string),
                Static.game.getPlayer().getName());

        // Set the defeat text
        setDefeatText();

        // Set the listeners
        findViewById(R.id.b_start_over).setOnClickListener(this);

        // Delete the game file
        deleteGameFile();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.defeat, menu);
        return true;
    }

    @Override
    protected void onRestoreInstanceState(Bundle bundle) {
        super.onRestoreInstanceState(bundle);
        this.defeatText = bundle.getString(DEFEAT_TEXT_KEY);
    }

    @Override
    protected void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString(DEFEAT_TEXT_KEY, this.defeatText);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.b_start_over:
                startOver();
                break;
            default:
                break;
        }
    }

    /**
     * Deletes the saved game file.
     */
    private void deleteGameFile() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Deleting game file");
        deleteFile(Static.CURRENT_GAME_FILE_NAME);
    }

    /**
     * Updates the defeat TextView with the defeatText.
     */
    private void setDefeatText() {
        this.defeat.setText(this.defeatText);
    }

    /**
     * Go back to the very first Activity.
     */
    private void startOver() {
        if (BuildConfig.DEBUG) Log.d(TAG, "Starting over");
        StartGameActivity.startStartGameActivity(this);
        finish();
    }

    /**
     * This is the preferred was to start a DefeatActivity. It creates the Intent required and fills it with the needed
     * values.
     *
     * @param context This is the parent context.
     */
    public static void startDefeatActivity(Context context) {
        if (BuildConfig.DEBUG) Log.d(TAG, "Request start DefeatActivity");

        // Use the provided context to start the defeat activity
        context.startActivity(new Intent(context, DefeatActivity.class));
    }
}
